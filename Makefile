# Compile or clean build files
LATEXCOMPILER=latexmk
LATEXMKOPTIONS=
LATEXMKOUT= -pdf

.PHONY: all
all: thesis defense abstract

thesis: master.pdf
master.pdf: master.tex Chapters/Chapter*.tex
abstract: abstract_eng.pdf abstract_cat.pdf
defense: defense.pdf
clean-thesis: clean-master
clean-abstract: clean-abstract_eng clean-abstract_cat


%.pdf: %.tex
	$(LATEXCOMPILER) $(LATEXMKOPTIONS) $(LATEXMKOUT) $<


clean-%: %.tex
	$(LATEXCOMPILER) -c $<

clean-all:
	$(RM) *.aux *.bbl *.blg *-blx.bib *.fdb_latexmk *.fls *.lof *.log *.lot *.nav *.out *.run.xml *.snm *.toc *.vrb Chapters/*.aux Appendices/*.aux *.pdf

.PHONY: clean-%
.PHONY: clean-thesis
.PHONY: clean-abstract
.PHONY: clean-all
