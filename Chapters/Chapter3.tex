% Chapter 3

\chapter{Drift-Diffusion Simulation of 2D based devices}

\label{driftdifusion}

\section{Introduction}

The simulation of electronic devices can be achieved through different models and techniques. In this chapter, the
Finite Element Method was chosen so that the geometry of realistic 2D-channel devices could be taken into account
as well as model the different novel materials used in Field Effect Transistors such as HfO$_2$ and MoS$_2$. This
kind of devices shows a behaviour that follows the drift-diffusion model as opposed to ballistic
devices such as the ones studied in chapter~\ref{ggraphite}.

This chapter is structured as follows: first, the semiconductor equations reviewed. Then, the material modelling for
the use in the FEM software, the minimal set of parameters needed to describe the material. Finally, results from the
studied devices as well as some improvements on the results presented in~\cite{FJovell2014TFM}. This chapter is a
continuation of the work done in Ref.~\cite{FJovell2014TFM}.

\section{Semiconductor Equations}
The basic equations that describe the behaviour of a semiconductor at the microscopic level are presented in this
section using quantum mechanics, statistical mechanics and few classical arguments. Throughout this section, the
steady state will be considered, hence all time derivatives will vanish. This model describes the behaviour of carriers
in a semiconductor in a zero or small electric field.


\subsection{Poisson's Equation, integral form}
Poisson's equation allows the calculation of the electric potential as it relates to the charge distribution.
\begin{align}
  \nabla \cdot\left[ \varepsilon~\nabla\phi \right] = -q \left(C -n + p\right),
  \label{eq:poisson}
\end{align}
Where $\phi$ is the electrostatic potential, $q$ is the elemental charge, $\varepsilon$ is the dielectric constant, $n$ and
$p$ the intrinsic, electron and hole concentrations and $C$ the concentration of ionized donors and aceptors. The electric
field is related to the electrostatic potential by
\begin{align}
    \nabla \times \textbf{E} = 0 ~\Rightarrow~ \textbf{E} = -\nabla\phi
\end{align}
where $\textbf{E}$ is the electric field and, by definition, is a conservative field. It is assumed and required that the solution of
Poisson's is continuous up to the second derivative, $\phi \in C_2$. This equation can also be written in temrs of integrals as well,
also called the weak form. This is useful for the Finite Element Method Eq.~\ref{eq:intweightedresiduals}. The dielectric tensor,
$\epsilon$, is assumed to be constant in these materials as they are homogeneous and isotropic so it can be taken as just a constant.
\begin{align}
 \nabla^2 \phi = - \frac{\rho(\textbf{r})}{\varepsilon}
\end{align}
where $\rho (\textbf{r}) = q \left(C -n + p\right)$ and $n$ and $p$ are functions of the position. Given some arbitrary weight
function $w(\textbf{r})$ and multiplying at both sides and integrating over all the volume
\begin{align}
  \int_V w(\textbf{r}) \nabla^2 \phi ~\diff V = - \int_V w(\textbf{r}) \rho(\textbf{r}) \diff V.
\end{align}
Now, applying Green's Theorem \cite{Green1854} to the left hand side of the equation and the differential vector identity
\begin{align}
  w(\textbf{r})\nabla^2 \phi = \nabla \cdot (w(\textbf{r}) \nabla\phi)- \nabla w(\textbf{r})\nabla\phi
\end{align}
the following expression is obtained
\begin{align}
  \int_{\partial V} \left( w(\textbf{r}) \cdot \nabla\phi \right)~\diff{S} -
  \int_V \left(\nabla w(\textbf{r})\right)\left(\nabla\phi\right)~\diff{V}
  = -\int_V w(\textbf{r}) \rho(\textbf{r})~\diff{V},
\end{align}
which is indeed an integral form of Eq.~\ref{eq:poisson}. Now, the solution $\phi$ is only required to be derivable up to the first
derivative, $\phi \in C_1$, but so is the weight function, $w \in C_1$ as well. Hence the name ``weak'' form as this is a weaker statement
with respect to its differential form.

\subsection{Continuity Equations}

In the steady state solutions, the continuity equations on a semiconductor are
\begin{subequations}
    \begin{align}
        \nabla \cdot J_n &= - q \left( R_n - G_n \right), \\
        \nabla \cdot J_p &= + q \left( R_p - G_p \right).
    \end{align}
    \label{eq:continuity}
\end{subequations}
where $J_{n,p}$ are the density currents for electrons and holes, $q$ the elemental charge, $G_{n,p}$ and $R_{n,p}$ the
generation and recombination rates for electrons and holes respectively. Supplemented by the constitutive relations for
the current, the following pair of equations is obtained
\begin{subequations}
    \begin{align}
        J_n &= qn\mu_n \nabla\phi + q D_n \nabla n, \\
        J_p &= qp\mu_p \nabla\phi - q D_p \nabla p,
    \end{align}
    \label{eq:current}
\end{subequations}
$\mu_{n,p}$ the electron and hole mobilities and $D_{n,p}$ the diffusion coeficient. Equations~\ref{eq:continuity} and
Eq.~\ref{eq:current} along with the Poisson's Equation, Eq.~\ref{eq:poisson}, form the so-called drift-diffusion model
for semiconductors.

\subsection{Carrier Densities}
In a semiconductor, one of the most important quantities is the carrier density $n$ and $p$. From statistical mechanics
and solid state physics, these equations can be derived.
\begin{subequations}
    \begin{align}
        n =  N_C~\mathcal{F}_{\nicefrac{1}{2}}\left(\frac{E_f-E_c}{k_B T}\right) \\
        p =  N_V~\mathcal{F}_{\nicefrac{1}{2}}\left(\frac{E_v-E_f}{k_B T}\right)
    \end{align}
    \label{eq:carrierdensities}
\end{subequations}
where $N_C$ and $N_V$ are the corresponding effective density of states for the conduction and the valence bands.
The $\mathcal{F}_{\nicefrac{1}{2}}$ is the Fermi integral function of order $\nicefrac{1}{2}$,
\begin{align}
    \mathcal{F}_{\nicefrac{1}{2}}(x) = \int_0^\infty \frac{t^{\nicefrac{1}{2}}}{1+\exp(t-x)}\diff t.
    \label{eq:fermidirac}
\end{align}
The effective density of states for the conduction and valence bands are calculated from the expressions
\begin{subequations}
    \begin{align}
        N_c = 2 \left(\frac{2 \pi {m_e}^* k_B T}{h^2}\right)^{3/2}\\
        N_v = 2 \left(\frac{2 \pi {m_h}^* k_B T}{h^2}\right)^{3/2}
    \end{align}
\end{subequations}
where $m_{e,h}^*$ are the effective density of states masses for electrons and holes in the material, $k_B$ the Boltzmann constant,
$T$ temperature and $h$ the Planck constant.

\subsection{Material Modelling}
For the simulations run on these devices, three different kinds of materials were defined: semiconductors, conductors and insulators.
Every one of these regions is treated different by the simulator. To simulate the different materials, a set
of parameters were introduced that are characteristic of the materials. For semiconductors, the permittivity $\epsilon$, the electron
affinity $\chi$, the bandgap $E_g$, the effective mass for electrons and holes $m^*$ and the mobility for the two different carriers
were defined. For conductors, the workfunction $\Phi$ and for insulators the permittivity, the electronic affinity band gap $E_g$ were
defined as well. In the case of semiconductors, constant mobilities were chosen as the bias applied in the different simulations were
small and all the simulations were carried out at room temperature (300K).

\section{Device under Study}
The FEM, in conjunction with the drift-diffusion equations~\ref{eq:poisson}, \ref{eq:current} and \ref{eq:carrierdensities} allow for
the study of electronic devices of larger dimensions with respect to the small DFT devices or the description of MD\footnote{Parts of
this section is a continuation on the Master's Thesis~\cite{FJovell2014TFM}. Many of the details missing here of that work can
be found there.}. This also is useful to calculate macroscopic quantities that only have meaning in a macroscopic level. On top of this,
some device features can also be calculated in a way that there is no need for an atomistic description of the system which would otherwise
be very computationally expensive. First, a Field Effect Transistor (FET) based on a 2D MoS$_2$ channel is shown as part of the continuation on
previous work. Then, a MoS$_2$ $p - n$ junction used to study the depletion length as a function of the doping.

\subsection{2D MoS$_2$ channel FET}
The FET geometry used for this work is based on from Ref.~\cite{mos2transistor}. A sketch not to scale is shown in Fig.~\ref{fig:expfetmos2}.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{exp_mos2_transistor}
    \caption{Single layer MoS$_2$ transistor geometry, not to scale. Reprinted with permission from Springer~\cite{mos2transistor}.}
    \label{fig:expfetmos2}
\end{figure}
The gate oxide is HfO$_2$, with ohmic gold contacts with a degenerate silicon separated by a SiO$_2$ insulator substrate
that serves as a back gate and finally the MoS$_2$ channel. The output characteristics for this MoS$_2$ transistor are shown
in Fig.~\ref{fig:mos2results}. The set of parameters that characterize each material used are in table~\ref{table:mosfetparams}.
\begin{table}
    \hspace{\fill}
    \begin{tabular}[t]{ |c|c| } \hline
        Sc & MoS$_2$ \\ \hline
        $\varepsilon$ & 4$~\varepsilon_0$ \\
        $\chi$ & 6.0 eV \\
        E$_g$ & 1.9 eV \\
        $m^*_e$ & 0.54 $m_e$ \\
        $m^*_h$ & 0.44 $m_e$ \\
        $\mu_n$ & 217 cm$^2 V^{-1} s^{-1}$ \\
        $\mu_p$ & 40 cm$^2 V^{-1} s^{-1}$ \\ \hline
    \end{tabular}
    \hspace{0.1cm}
    \begin{tabular}[t]{|c|c|} \hline
        Oxide & HfO$_2$ \\ \hline
        $\varepsilon$ & 25$~\varepsilon_0$ \\
        $\chi$ & 2.1 eV \\
        E$_g$ & 5.7 eV \\ \hline
    \end{tabular}
    \hspace{0.1cm}
    \begin{tabular}[t]{|c|c|} \hline
        Metal & Au \\ \hline
        $\Phi$ & 5.45 eV\\ \hline
    \end{tabular}
    \hspace{\fill}
    \caption{Modelling parameters for MoS$_2$ (Sc), HfO$_2$ (Oxide) and Gold (Metal). Permitivitty $\varepsilon$, Electronic Affinity $\chi$,
    Energy Band Gap $E_g$ (300K), Effective mass for electrons and holes $m^*_{e,h}$ and mobility for electrones and holes $\mu_{e,h}$.
    For the metal, the workfunction $\Psi$ is only needed.}
    \label{table:mosfetparams}
\end{table}
Finally, with all these parameters, the output characteristics of the transistor are shown in Fig.~\ref{fig:mos2results}.
\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{Ids_vs_Vg_mos2mosfet.png}
    \caption{Simulated single-Layer MoS$_2$ FET output characteristics.}
    \label{fig:mos2results}
\end{figure}
Comparing these results with the experimental results in Ref.~\cite{mos2transistor} it can be observed that the saturation current
obtained by the simulations is about 10 times the experimental data. This can be corrected by changing the electronic affinity of
the MoS$_2$ channel. This way, while obtaining accurate agreement with the experimental results, some of these quantities can lose
its physical meaning as these are not the experimental values anymore. On the other hand, the experimental value of the subthreshold
swing is 74 mV dec$^{-1}$, while the obtained by the simulations is around 65 mV dec$^{-1}$. According to~\cite{Sze2006}, the
subthreshold swing is given by
\begin{align}
    S_{s} = \ln(10) \frac{k_B T}{q} \left( 1+ \frac{C_q}{C_{ox}}\right).
\end{align}
With $k_B$ is the Boltzmann constant, $T$ the temperature, $q$ the electron charge, $C_q$ the quantum capacity of the depletion layer
and $C_{ox}$ the capacity of the oxide. So in theory is it possible to tune the values of the capacitance of the oxide
or the depletion layer, but would be by altering the geometry of the device or adding artificial values of the materials in the
simulation.

\subsection{MoS$_2$ $p - n$ junction}
The geometry used for this study is a simple metal-semiconductor-semiconductor-metal structure surrounded by vacuum as shown in
Fig.~\ref{fig:mos2pn}. For this, different channel widths and different dopings for the MoS$_2$ were studied. The objective is to study
the different depletion lenghts obtained using this model under a zero bias and under a small electric field.
\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{mos2_junction_diagram}
    \includegraphics[width=0.9\linewidth]{mos2_junction_chargeconc_2}
    \caption{Top: Schematic MoS$_2$ $p - n$ junction of width 10 $\mu$m. Three different regions can be distinguished: Vacuum (Yellow), Metal
    contacts, gold (Blue) and MoS$_2$ (Purple).
    Bottom: MoS$_2$ junction charge concentration. Five different concentrations were used: $1.0\times10^{10}$ cm$^{-2}$ (red),
    $5.0\times10^{10}$ cm$^{-2}$ (light blue), $1.0\times10^{11}$ cm$^{-2}$ (green), $5.0\times10^{11}$ cm$^{-2}$ (yellow), $1.0\times10^{12}$
    cm$^{-2}$ (dark blue).}
    \label{fig:mos2pn}
\end{figure}
MoS$_2$ regions were modeled using the same parameters in table~\ref{table:mosfetparams}. The metal-semiconductor interface created a Schottky barrier,
which was taken into account by the simulation. In one case, the interface was considered an Ohmic contact instead. Table~\ref{table:wvsxp} shows
the results of the depletion lengths for different channel widths and different semiconductor doping levels.
\begin{table}[h!]
    \centering
    \begin{tabular}{ |c|ccccc| } \hline
        L$_c$ $\backslash$ doping & $1.0\times10^{10}$ & $5.0\times10^{10}$ & $1.0\times10^{11}$ & $5.0\times10^{11}$ & $1.0\times10^{12}$ \\ \hline
        0.1 $\mu$m &        &        &       & 10 nm & 10 nm \\ \hline
        1 $\mu$m   &        &        & 50 nm &       &      \\ \hline
        10 $\mu$m  & 400 nm & 100 nm & 50 nm & 14 nm & 9 nm \\ \hline
        100 $\mu$m & 600 nm &        &       &       &      \\ \hline
        % 1 nm       &        &        &       &       &      \\ \hline
        10 $\mu$m$^*$  & 600 nm &        &       &       &      \\ \hline
    \end{tabular}
    \caption{Depletion width (x$_p$) as a function of the channel length (L$_c$) at different doping levels (cm$^{-2}$) with Gold Shottky contacts.
    The blank spaces left represent flat depletion zone. $^*$This corresponds to a simulation with Gold Ohmic contacts instead.}
    \label{table:wvsxp}
\end{table}
In Fig.~\ref{fig:wvsxp} show the same results as table~\ref{table:wvsxp} plus a comparison of the bulk depletion width with similar doping
of a $p-n$ junction. Of course, the depletion widths for the 2D $p-n$ junction behave differently from the 2D counterpart due to the
dimensionality of the material. These results also suggest that another set of equations should be derived for 2D materials taking into a
account the the thickness of the material.
\begin{figure}
    \centering
    \includegraphics{xp_vs_n}
    \caption{Table~\ref{table:wvsxp} results with bulk comparison. Depletion width as a function of the 2D doping level (purple).
    The other lines represnt the different depletion widths for bulk semiconductors with similar doping levels.}
    \label{fig:wvsxp}
\end{figure}
In Fig.~\ref{fig:junctionpotential} the potential profile is shown for the 10$\mu$m $p-n$ junction.
\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{mos2_junction_contourpotential_1}
    \includegraphics[width=0.75\linewidth]{mos2_junction_contourpotential_2}
    \includegraphics[width=0.75\linewidth]{mos2_junction_potential}
    \caption{Potential contour of the MoS$_2$ $p-n$ junction of width 10$\mu$m at zero bias.}
    \label{fig:junctionpotential}
\end{figure}

The mesh is one of the key elements in order to obtain precise and reliable simulation results. The mesh is defined as a
collection of points which the solution will be computed on until in converges. However, it is not a good idea to use a
highly dense mesh for various reasons. First, there has to be a balance between accuracy and performance. Secondly, there
has to be enough nodes near the interface of different materials to capture the details with accuracy. Finally, in the
regions far from these interfaces, the mesh should be sparse as to not waste computational resources in parts that are not
relevant for the outcome of the simulation.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{mesh_metal-mos2_interface.png}
    \caption{Detail of the metal-MoS$_2$ interface mesh. The green lines represent the division of each "element" of the graph
    that represents the mesh. Regions: Vacuum (Yellow), Metal contacts, gold (Blue) and MoS$_2$ (Purple)}
    \label{fig:meshmemos2}
\end{figure}

\section{Conclusions}
Two different devices were studied withing this model giving insight to the modelisation of electronic devinces using the
drift-diffusion equations with a finite element scheme. The single-layer MoS$_2$ FET was characterized using the FEM implemented
in the SILVACO TCAD suite. This simulator assumed that all the regions in the device will behave like bulk material and
all physical equations describing these regions are implemented like so. Plus, small size effects cannot be taken into
account in order to simulate nanometric-sized systems properly. However, the parameter set of these regions can be tweaked
to reproduce at a qualitative level some of the output characteristics of the device. The output characteristics of this
device is in accordance with the experimental data in a qualitative level. In order to adjust the output characteristics
so that the simulated device reproduces more accurately the experimental data, few tweaks on the material properties must
be changed. A MoS$_2$ $p-n$ junction was also studied within this framework to calculate depletion lengths of this system.
These results will help in the study of 2D materials with the drift-diffusion model. Different widths with different dopings
were considered obtaining the different depletion zones values. Compared with the bulk results, single layer MoS$_2$ behaves
differently and these results suggest that the low dimensionality of the material is highly relevant in the drift-diffusion model.
