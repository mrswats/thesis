% Chapter 4

\chapter{Bond Order Potential parametrisation for Pd-C and Ni-C}

\label{moleculardynamics}

\section{Introduction}
The interaction between carbon based materials and some metals is very important since the rise of
graphene as a new material. The electronic transport through structures based on these materials
could help both understand its physics and figure out if such metals are suitable for high frequency
devices along with graphene in different structures and geometries.

Albe \emph{et al.} \cite{Albe2002, Erhart2005, Nord2003} parametrized elements metals of interest
for many applications. These include Carbon, Platinum, Gallium, Nitrogen and Silicon. This was
developed after the potential that Tersoff \cite{Tersoff1986} proposed in 1986 and with the later
modifications of Brenner \cite{Brenner2002}, it is a formalism that is widely used nowadays and it
is shown in this chapter.

The objective of this project is to work in this framework with relevant materials for radiofrequency
applications and that have not been yet parametrized. In this project the parametrization for Nickel
and Palladium was studied. The interest on these materials is such that it will allow to construct
different structures and use molecular dynamics to perform both a CVD-like deposition or structural
relaxation of transport structures as a first step for DFT calculations. Building realistic structres
could give a more accurate description of contact resistance for top-contact like geometries, similar
to the ones studied in chapter~\ref{ggraphite}. In turn, this parametrisation will be useful for geometry
relaxation of the structures containing these elements for further study afterwards with other methods,

In particular Nickel and Palladium were studied in detail to obtain a parametrization of these materials
for a handful of reasons. For nickel it is known that its (111) surface has a lattice parameter very similar to
graphene and it is very interesting to use it in conjunction with. On the other hand, Palladium is
a material proven to be very useful in high frequency applications~\cite{HFgrapheneAmpl, grapheneHFelectronics}.

\subsection{The Bond Order Potential \label{ssec:bop}}
First proposed by Tersoff \cite{Tersoff1986}, this empirical interatomic potential was the first of
the kind to take into account more terms in an energy expansion such that three body interactions were
possible, leading to the prediction of closed packaged structures~\cite{Tersoff1988}. Later,
Brenner~\cite{Brenner2002} refined this potential for the study of chemical reactions in hydrocarbons.
Afterwards, Albe \emph{et. al.} \cite{Albe2002, Erhart2005} used this same approach with the help
of the Tight Binding potential for transition metals of Cleri and Rosato \cite{Cleri1991}. With
this last tuning it allowed to study of mixed systems with carbon and \emph{d}-transition metals.

From Ref.~\cite{Albe2002} the total energy is calculated using the following expression
\begin{align}
    E = \sum_{i>j} f_{ij}(r_{ij}) \left[ V^{ij}_R(r_{ij}) - \frac{b_{ij}+b_{ji}}{2} V^{ij}_A(r_{ij})\right],
\label{eq:total_E}
\end{align}
where $V_R$ and $V_A$ are the repulsive and attractive potentials. The approximation used includes
only nearest neighbours and in order to be computationally efficient a cutoff function is required.
The cutoff function, $f(r)$, takes the following form
\begin{align}
    f(r) =
    \begin{cases}
        1, &r \leq R-D, \\
        \frac{1}{2}-\frac{1}{2} \sin\{ \pi (r-R)/2D \},  &\vert R-r \vert \leq D, \\
        0, &r \geq R+D \\
    \end{cases}
    \label{eq:cutoff}
\end{align}
Where $r$ is the distance of two atoms labelled $i,j$ and $R$ and $D$ are parameters to be determined later.
The $b_{ij}$ coefficients are the bond order coefficients which include angular dependency to reproduce correctly
the formation of bonds. Following the Albe~\textit{et al} paper, this term takes the following form
\begin{gather*}
    b_{ij} = (1+ \chi_{ij})^{-1/2}\\
    \chi_{ij} = \sum_{k(\neq i,j)}\,f_{ik}(r_{ik})\,g_{ik}(\theta_{ijk})\,\exp\left[{2\mu_{ik}(r_{ij}-r_{ik})}\right]
\end{gather*}
where the cutoff function is included again. Finally, the angluar dependancy is
\begin{align}
    g(\theta) = \gamma \left( 1+ \frac{c^2}{d^2} - \frac{c^2}{[d^2+(1+\cos{\theta})^2]}\right).
\end{align}
For $c=0$ this term is equal to a constant, $\gamma$, and the potential takes the form of the
Embeded Atom Method potential~\cite{Erhart2005}. This angular dependency is decisive for covalent systems and for
modeling metals as well. Finally the analytical form of the attraction and repulsion potentials in
Eq.~\ref{eq:total_E} take the form of fully equivalent Morse-like structures
\begin{align}
    \nonumber
    V_R(r) &= \frac{D_0}{S-1}\exp\left[ -\beta \sqrt{2 S}\,\,(r-r_0)\right],\\
    V_A(r) &=  \frac{S\,D_0}{S-1}\exp\left[ -\beta \sqrt{2/S}\,\,(r-r_0)\right],
\end{align}
with $D_0$ the dimer binding energy and $r_0$ the equilibrium distance. If the binding energy $D_0$
and the ground state frequency of the dimer molecule are known, then $\beta$ is obtained from the expression
\begin{align}
    \beta = k \frac{2\,\pi\,c}{\sqrt{2 D_0 / \mu}} ,
\end{align}
where $k$ is the wave number and $\mu$ the reduced mass. The parameter $S$  can be
determined by the Pauling criterion that relates the bonding distance $r_b$ and
the bond energy
\begin{align}
    E_b = - D_0 \exp\left[ - \beta \sqrt{2\,S} (r_b-r_0)\right].
\end{align}
This last equation must be fullfilled when fitting lattice parameters and cohesive energies. This
is extremely decisive for the transferability of the potential.

To summarize this section, the set of parameters that describe the physics of the simulated systems is
\begin{align}
    \left\{S, \beta, D_0, r_0, R, D, \gamma, c, d, \mu \right\}.
\end{align}

\section{Parameter Optimisation}
To obtain all ten parameters from the BOP potential an optimisation of the parameters has to be carried out from the
DFT total energy calculations. For this, a geometric relaxation has to be done with DFT to obtain both final geometry and
total energy. With this, the Parallel Tempering Monte Carlo algorithm is able to find a set of parameters that describes
both the geometry and the energy with a certain degree of accuracy.

The basic idea of the PTMC algorithm is to run several Monte-Carlo simulations at different temperatures for different
sets of parameters at each temperature. This allows the exchange of parameters between different temperatures, to
thorougly explore the configuration space of the parameters at higher temperatures, while finding the local minima at the
lower temperatures. Represented in Fig.~\ref{fig:PTMC} the basic principle of the algorithm is shown. This method is
demonstrated to be useful when the dimensionality of the parameter space is large and has been used before in
Refs.~\cite{Forster2015Thesis, Forster2015} to solve a very similar problem.

\begin{figure}[h]
    \centering
    \includegraphics{PTMC_diagram_forster_thesis}
    \caption{Principle of PTMC. Markov chains at high temperatures allow to explore
    broad regions of configuration space and help the chains at lower temperatures
    to escape local minima though successive swapping moves, resulting in a much
    faster equilibration. In the context of global optimization, the low temperature
    replicas can also discover previously unexplored regions much more efficiently.
    Source~\cite{Forster2015}.}
    \label{fig:PTMC}
\end{figure}

In order to use the PTMC algorithm, several test configurations with their respective energies must
be known beforehand. Several configurations had to be relaxed using DFT to obtain the total energy
of the system, according the respective functional used, and the relaxed geometry. The simplest
but most significant system that was studied are a series of nanoparticles of increasing size in
number of atoms and bonds with the substrate. In particular we were interested in studying the
bonding of Nickel and Palladium on a graphene substrate described in the next section.

\subsection{Geometry Description}
In order to use the parametrisation method described in the previous section, some training input data had to be provided.
This data came from DFT calculations, in particular metal nanoparticles on top a graphene layer were chosen.
In Fig.~\ref{fig:MeonG} one of the nanoparticles is shown. Several sizes and several positions on top of the different sites
on graphene were tested to find the optimal place for metal atoms in the substrate to rest at minimum energy.
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.75\linewidth]{ptmc_geometries/Ni10_lateral.jpg}
  \caption{Example of a small nanoparticle of 10 Ni atoms over a graphene sheet.}
  \label{fig:MeonG}
\end{figure}
The complete set of geometries used for the PTMC algorithm training range from a single atom on top of the graphene lattice
in a hollow, bridge or top position, up to a 55 atom nanoparticle. From 1, 2, 3, 4 and 7 metal atoms on top of graphene consist
on planar structures layed on the most stable positions, see Fig.~\ref{fig:12347}.
\begin{figure}
    \centering
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd1_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd2_top.jpg}
    \vspace{0.5cm}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd3_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd4_top.jpg}
    \vspace{0.5cm}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd7_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd7_lateral.jpg}
    \caption{1, 2, 3, 4 and 7 (top and lateral view) Pd atoms on top of a graphene sheet after geometry relaxation.}
    \label{fig:12347}
\end{figure}
Then, a geometry composed by 13 atoms in three different internal configurations represented in Fig.~\ref{fig:13abc}. The number
of Carbon-Metal bonds differs. The configuration $a$ and $b$ have 3 while configuration $c$ has 4.
\begin{figure}
    \centering
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd13a_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd13a_lateral.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd13b_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd13b_lateral.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd13c_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd13c_lateral.jpg}
    \caption{Top and lateral view for 13 Pd atoms on top of a graphene sheet with three different configurations.}
    \label{fig:13abc}
\end{figure}
The 38 atom nanoparticle is shown in Fig.~\ref{fig:38ab} with two internal different structures, represented by a mirror symmetry.
\begin{figure}
    \centering
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd38a_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd38a_lateral.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd38b_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd38b_lateral.jpg}
    \caption{Top and lateral view for Pd 38 a and b configurations on top of a graphene sheet. The two configurations are a mirror image of each other.}
    \label{fig:38ab}
\end{figure}
Finally, a 55 atom ball represented in Fig.~\ref{fig:55atom}.
\begin{figure}
    \centering
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd55_top.jpg}
    \includegraphics[width=0.49\linewidth]{ptmc_geometries/Pd55_lateral.jpg}
    \caption{Top and lateral view for Pd nanoparticle consisting of 55 Pd atoms arranged in a quasisphere.}
    \label{fig:55atom}
\end{figure}
In Appendix~\ref{ptmcgeometries} the rest of the Nickel-Graphene geometries are shown in a similar fashon for completeness.

\subsection{Computational Details}
% Unsure about this whole section? Maybe can be put into the previous section
All the geometries were relaxed until all constrained forces were lower than $0.01$ eV/\AA\ using the GGA functional
in the parametriation of Perdew, Burke and Ernzerhof~\cite{gga-pbe} for Nickel. For Palladium, the LDA functional~\cite{LDA}
was used instead as it gives a better description of the Pd-C bond distance~\cite{grapheneonmetals, Wintterlin2009}.
The core electrons were treated using norm-conserving pseudopotentials of the Troullier-Martins type~\cite{pseudos}
with a double-$\zeta$ plus polarization basis taking into account spin polarization for Nickel. All this calculations
used a 2$\times$2$\times$1 Monkhorst-Pack Grid~\cite{MonkhorstPack} using the {\sc Siesta} package~\cite{siesta}.

\subsection{Force field Parameters}
% Results should be a table with the parameters and maybe an interpretation of those.
After the optimisation of parameters through the PTMC algorithm, a set of parameters was found for both Ni-C and Pd-C,
summarized in table~\ref{table:params}. This set of rules describes only the Carbon-Metal interaction. To describe the
Carbon-Carbon pair the description in Brenner~\cite{carboncarbon} is used and the parametrisation within. For the
Metal-Metal interaction the Embeded Atom Potential is used as described in Ref.~\cite{metalmetal}. For completeness, added
in Appendix~\ref{lammpstersoff} the required LAMMPS potential file as well as a code snippet for how to include them in
any LAMMPS calculation. In these files the Pd-Pd, Ni-Ni and C-C interactions are also included as described above.
\begin{table}[h!]
    \centering
    \begin{tabular}{ |c|c|c| }
            \hline
                     & Pd-C                 & Ni-C                 \\ \hline
            $S$      & 1.433$\times10^{2}$ & 1.432$\times10^2$ \\
            $\beta$  & 4.300 & 4.296 \\
            $D_0$    & 1.758 & 1.758 \\
            $r_0$    & 6.802$\times10^{-2}$ & 6.807$\times10^{-2}$ \\
            $R$      & 5.141 & 5.150 \\
            $D$      & 9.89$\times10^{-3}$ & 5.0$\times10^{-2}$ \\
            $\gamma$ & 1.806$\times10^{-2}$ & 1.800$\times10^{-2}$ \\
            $c$      & 6.903$\times10^2$ & 6.888$\times10^2$ \\
            $d$      & 4.755 & 4.753 \\
            $\mu$    & 2.803 & 2.807 \\
            \hline
    \end{tabular}
    \caption{Bond Order Potential parameters obtained from the PTMC optimisation through \textit{ab initio}
    training for the Pd-C and Ni-C pairs. The C-C and Me-Me parameters are added in Appendix~\ref{lammpstersoff}.}
    \label{table:params}
\end{table}
For each geometry, the PTMC algorithm reached an energy reproduction according to the parameters used. In table~\ref{table:energies}
the values for the obtained DFT adsorption energies and the reproduced energies with the PTMC algorithm for each set of parameters
is shown side by side.
\begin{table}[h!]
    \centering
    \begin{tabular}{ |c|cc|cc| } \hline
        N$_a$  &   $E_\text{target}$(Pd) & $E_\text{reproduced}$(Pd) & $E_\text{target}$(Ni) & $E_\text{reproduced}$(Ni) \\ \hline
        1      &  -4.39458 &  -4.02778 & -3.88876 & -3.93083 \\ \hline
        2      &  -5.40951 &  -5.29990 & -4.25306 & -5.50767 \\ \hline
        3      &  -6.47832 &  -6.14807 & -5.20541 & -6.64512 \\ \hline
        4      &  -6.03174 &  -6.36230 & -5.68362 & -6.94597 \\ \hline
        7      & -10.52663 &  -9.34759 & -7.44564 & -9.06748 \\ \hline
        10     &  -9.44840 &  -9.75142 & -7.95338 & -9.00257 \\ \hline
        13a    &  -6.71057 &  -9.31021 & -5.45038 & -7.10955 \\ \hline
        13b    &  -6.52372 &  -5.86262 & -7.25059 & -8.69627 \\ \hline
        13c    &  -8.73059 &  -9.15725 & -7.99246 & -9.33724 \\ \hline
        38a    &  -9.14434 &  -9.24890 & -7.44934 & -7.39335 \\ \hline
        38b    &  -6.49479 &  -6.99212 & -5.14229 & -6.47318 \\ \hline
        55     &  -9.56981 & -10.86422 & -7.20522 & -8.06162 \\ \hline
    \end{tabular}
    \caption{Adsorption and reproduced energies (in eV) for different configurations of metallic nanoparticles on a graphene sheet
    for both Palladium (Pd) and Nickel (Ni). The letters by some of the structures indicate different internal configurations,
    see Appendix~\ref{ptmcgeometries}.}
    \label{table:energies}
\end{table}
These results look very promising as the reproduced energies are very close to the target energies but more testing has to be
done before using this force field's parameters for any production simulation. For example, the dimer energy, equilibrium distance,
or the bond distance are all measures of how good of a parameter set this is with the possiblity of iterating the parameters again
with a new initial guess.
%TODO: Add some tests, even if they are numnerical or small tests and an explanation of the huge differences for the Ni

\section{Conclusions}
Of course, this parameter set needs more testing before it can be released and needs to be validated agains
experimental data and theoretical calulation from first principles. This said, a realistic structure of metal
deposited on a graphene sheet can be carried out and its results studied. This simulations can be then used
in combination with tight-binding codes to calculate ballistic transport properties in the context of high
frequency applications.

To conclude, the bond order potential in the interpretation of Albe \textit{et. al.} is the most suitable
force field to describe the Metal-Carbon interaction and a set of parameters was found for the interactions
Pd-C and Ni-C. The parametrisation of Pd-C and Ni-C interactions were studied for the suitability of metallic
contacts with graphene using the PTMC algorithm for parameter optimisation. This algorithm is a very efficient
tool for optimisation tasks that allows for a thorough search of the configuration space while able to find
optimal values on a certain region of this space. With this parameter set, it will be possible to simulate CVD-like
deposition of a metal on graphene, or use molecular dynamics for a first relaxation of large systems which would
not be suitable using first principles techniques.
