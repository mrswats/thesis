% Chapter 5

\chapter{Graphite Graphene Contacts}

\label{ggraphite}

\section{Introduction}
Throughout the last fifteen years, graphene has demonstrated its capabilities as a new
material with its extraordinary properties~\cite{graphenereview}. Although the
lack of bandgap forbids the use of this material for digital applications, its
properties are very well suited for analog radiofrequency devices~\cite{electronic2dreview}.
However, before graphene can be widely adopted, several difficulties must be overcome.
In particular, one of the limitations for the use of graphene in analog electronics is
the high contact resistance when metal-graphene contacts are fabricated, while an
upper bound of $100~\Omega \cdot \mu$m would be desirable~\cite{electronic2dreview,rcbound}.
\\\\
Theoretical work has been carried out for contact resistance between graphene and
other metals. For instance, Chaves ~\textit{et al}~\cite{rcmodel} created a model for
contact resistance between metal and graphene in a top contact-like geometry.
The metal-graphene edge contact geometry, in spite of its vanishing contact
overlap, has also been proven to be at least as good as some of
the top contact geometries~\cite{sidecontact}.
Also, metal-carbon nanotube contacts have been long studied, both experimentally
and theoretically, with extensive reviews in Refs.~\cite{SchottkyMCNTs,MetalCNTReview}.

Since graphene is a semimetal, interface dipoles will quickly be screened out, and
thus no Schottky barriers are expected---or, more precisely, they are thin enough that
carriers may easily tunnel through---. So, of the three injection mechanisms discussed in
Ref.~\cite{review-contacts-2dsc}, only field emission should be applicable to lateral injection
into graphene. Phase engineering~\cite{phase-eng-1,phase-eng-2,phase-eng-3} is a promising
approach to achieve high-quality lateral contacts to semiconductors, but in principle not
applicable to graphene. In top contacts, charge is injected through an interface with a lower
degree of covalency than in edge contacts~\cite{SchulmanArnoldDas2018}. A possible strategy to lower
contact resistance is the addition of an interface layer~\cite{interfacial-layer}. Other strategies
have been reviewed in Ref.~\cite{rc-strategy}.

Despite their obvious similar structural properties, the use of graphite as an
electrode for contacting graphene has received much less attention. The Lieber
group has synthesized monolithic graphene-graphite structures, obtaining
specific contact resistivities in the range of 700-900~$\Omega \cdot \mu$m, better than
similarly fabricated Cr/Au junctions~\cite{ggmonolithic}. Also, Chari ~\textit{et al}
measured the resistivity of rotated graphite-graphene contacts, obtaining
specific contact resistivities as low as 133~$\Omega \cdot \mu$m for holes and
200~$\Omega \cdot \mu$m for electrons~\cite{ggcontactexp}.
\\\\
The objective of this paper is to demonstrate the viability of graphite-graphene
contacts and show their fundamental limits. To this purpose, we describe in
section~\ref{ssec:geometry} the used geometry, followed by the computational
methodology in section~\ref{ssec:computational}. We show in section~\ref{sec:results}
that this yields results well below the upper contact resistance limit for certain
values of the overlap area and doping level. Then, an eigenchannel analysis
give us more insight about the scattering processes in the interface between the
graphite substrate and the graphene. This analysis brings us to the conclusions,
in section~\ref{sec:conclusions}, that the graphite-graphene interface presents more of
an area effect than metal-graphene contacts~\cite{XCS}, but still with very
small transfer lengths of $\sim$20~\AA.



\section{Methodology}
\subsection{Geometry Description \label{ssec:geometry}}

Top contact geometries will be the focus because they are the most easily fabricated~\cite{review-contacts-2dsc}.
In Figure~\ref{fig:gggeometry}, a ball-and-stick representation of the structure of the graphite-graphene contact
is displayed. As usual in ballistic transport calculations, there are three differentiated zones: a left
electrode---where the electrons are injected---, a scattering zone through which electrons will pass or reflect,
and a right electrode into which electrons that were not backscattered will arrive. The electrodes are semi-infinite
and a single graphite-graphene contact will be studied.

\begin{figure}[t!]
  \centering
  \includegraphics[width=1.0\linewidth]{figure_1.pdf}
  \caption{Ball-and-stick structure for the Graphite-Graphene top contact.
  Green/dark gray (white/light gray) balls indicate the carbon (hydrogen) atoms.
  Also indicated are the three different regions: Left Electrode, Scattering Zone
  and Right Electrode used for transport calculations.
  Electrons are injected in the left electrode through the scattering zone into
  the right electrode. }
  \label{fig:gggeometry}
\end{figure}

In this chapter, the effect of overlap length of graphene over the graphite bulk has been studied. The numbers in the scattering
zone indicate the different number of overlapping C-pairs providing the contact between graphene and the graphite substrate.
We also studied the case where the last graphite layer turns into the graphene sheet, which we interpret as an edge graphite-graphene
contact~\cite{sidecontact}. We have always considered perfectly aligned graphite-graphene junctions, as the study of the rotated
contacts fabricated in Ref.~\cite{ggcontactexp} would result in computational cells too large to be treated with first-principles methods.

Structures were relaxed from first-principles using the {\sc Siesta} code~\cite{siesta}, an efficient implementation of the Density
Functional Theory using localized pseudo-atomic orbitals. Transport calculations were carried out using
{\sc TranSiesta}~\cite{transiesta,transiesta2}, which implements the Non-Equilibrium Green's Function formalism under the DFT as well.


\subsection{Computational Details \label{ssec:computational}}

Calculations were performed with a double-$\zeta$ polarized basis set, using norm-conserving pseudopotentials of the
Troullier-Martins type~\cite{pseudos}. The lateral periodicity of the structure was accounted for using a 16 $k_{\parallel}$-point
Monkhorst-Pack~\cite{MonkhorstPack} grid for structural relaxations, and a dense 3056 $k_{\parallel}$-point grid in order to
properly capture the fine details close to the Dirac point. Numerical integrals were carried out on a discretization
mesh equivalent to a cutoff of 250 Ry, which provides total energies for graphene and graphite converged to the few meV range.
\begin{figure}[t!]
  \centering
  \includegraphics[width=1.0\linewidth]{figure_2.pdf}
  \caption{Energy bands for graphite. Purple (green) lines are obtained
  with GGA-PBE (vdW-DRSLL) parametrization, and the structure is taken to be the
  same for both functionals. The clear blue line indicates
  the Fermi Level position.}
  \label{fig:verticalbands}
\end{figure}
The Generalized Gradient Approximation (GGA) in the parametrization of Perdew-Burke-Ernzerhof (PBE)~\cite{gga-pbe} was used
to describe exchange-correlation effects. GGA accurately describes the lattice parameter of graphene, but underestimates
the interlayer distance~\cite{vdw-rdsll} in graphite. This, of course, can be corrected with the use of van der Waals (vdW) type functionals.
It has been demonstrated that the parametrization of Dion-Rydberg-Schr\"oder-Langreth-Lundqvist (DRSLL) of the vdW interaction
provides a good description of the interlayer distance while slightly overestimating the in-plane lattice constant~\cite{vdw-rdsll,vdw-rdsll-2}.
Thus, all structural and cell relaxations for graphene and graphite were carried out with the vdW-DRSLL functional until residual
stress tensor components were below 1 kbar (forces were very close to zero because of the structural symmetry), obtaining an
interplane distance of 3.377~\AA\ for graphite, in good agreement with experimental values. On the other hand, the interlayer
binding energy we obtain with the vdW-DRSLL functional is 199~meV/atom, quite higher than experimental values~\cite{graphite-exp}
or even values obtained with plane waves with the same functional~\cite{vdw-rdsll}. Due to SIESTA's use of localized orbitals,
binding energies~\cite{siesta-surface} have a tendency to be higher than otherwise obtained. Unfortunately, since difficulties were
found to achieve electronic convergence with the vdW-DRSLL functional for the transport structures (cf. Figure~\ref{fig:gggeometry}),
the PBE functional was used to relax the in-plane positions until in-plane forces were below 0.04~eV/\AA, while keeping the
interplane distance to the vdW-DRSLL value. The PBE functional was used for transport calculations as well. From the transport
point of view, this is justified because, for a fixed geometry, the two functionals yield very similar energy dispersions. In
Fig.~\ref{fig:verticalbands} the energy bands comparing the two functionals for bulk graphite with the relaxed geometry are shown.
Around the Fermi level, the energy difference between the two curves is negligible, and therefore it is concluded that both
functionals provide a good description of the energy dispersion of the system. In particular, it must be stressed that the dispersion
along $z$, which is closely related to the interlayer coupling ({\it i.e.} transport) of the electronic states, is not affected
by the passage from the vdW-DRSLL functional to PBE.


\section{Results \label{sec:results}}

From the {\sc TranSiesta} calculations we obtain the energy-resolved specific conductance ({\it i.e.} the conductance per unit of
tranverse length) for the different structures.

In Figure~\ref{fig:conductance} the specific conductance, in units of $G_0= {e^2}/h$ over the transverse length of the calculation
cell ($a_t = 2.484$~\AA), is shown for the graphite-graphene top contact for different values of the overlap (cf. Figure~\ref{fig:gggeometry})
and the edge contact. The pristine graphene case---which provides the quantum limit for the conductance of the whole structure---
is shown as well for reference. We note that the difference between the edge contact and the pristine graphene limit is small,
suggesting that an edge, or large overlap, contact between graphite and graphene would provide a low contact resistance.
\begin{figure}[t!]
  \centering
  \includegraphics[width=1.0\linewidth]{figure_3.pdf}
  \caption{Specific Conductance of the Graphite-Graphene contacts per
  unit of lattice length.}
  \label{fig:conductance}
\end{figure}
Regarding the varying amount of overlap, we observe that, contrary to the metal-graphene case~\cite{XCS}, there is a noticeable
monotonic dependence of the conductance on the overlap width. This is a reflection of the weaker substrate-graphene $p_z - p_z$
coupling compared to the stronger $d-p_z$ coupling in metal substrates. Despite the weaker coupling, a relatively narrow overlap
of $\sim 20$~\AA\ suffices to achieve a conductance similar to metallic substrates for doped graphene (see Ref.~\cite{XCS}).
\begin{figure}[t!]
  \centering
  \includegraphics[width=1.0\linewidth]{figure_4.pdf}
  \caption{Specific Contact Resistance.
  (a) Thermal broadening at 300~K plus electron-hole puddle of 50~meV.
  (b) Thermal broadening at 300~K plus electron-hole puddle at 5~meV. }
  \label{fig:contactresistance}
\end{figure}
We now turn our attention to the (specific) contact resistance. It can be extracted from the calculated conductance of the whole
 graphite-graphene structure, $G_{gg}(E_{F})$, where $E_F$ is the Fermi level, and the calculated conductance of the pristine graphene layer,
 $G_{g}(E_{F})$. The contact resistance $R_c$ is then given by the following operation on the conductances:
\begin{eqnarray}
  R_c(E_{F}) = G^{-1}_{gg}(E_{F}) - G^{-1}_{g}(E_{F}).
  \label{eq:1}
\end{eqnarray}
Now, in order to calculate the zero bias contact resistance considering a thermal and gaussian electron-hole (e-h) puddle~\cite{ehpuddles}
broadening, we used:
\begin{eqnarray}
  R_c(E_F) =& \nonumber \\
  \nonumber
  &+~k_B T \left( \int \mkern-12mu \int~\frac{\exp{[(E-E')/k_B T]}}{1+\exp{[(E-E')/k_B T]^2}}\,
  G_{gg}(E)~w(E'-E_F;\eta)~\textrm{d}E~\textrm{d}E' \right)^{-1} \\
  \nonumber
  &-~k_B T \left( \int \mkern-12mu \int~\frac{\exp{[(E-E')/k_B T]}}{1+\exp{[(E-E')/k_B T]^2}} \,
  G_{g~}(E)~w(E'-E_F;\eta)~\textrm{d}E~\textrm{d}E' \right)^{-1},
  \label{eq:2}
\end{eqnarray}
where $w(E'-E_F; \eta)$ is the gaussian broadening function and $\eta$ the broadening paremeter, taken to be $50$~meV for SiO$_2$
substrates~\cite{ehpuddles}, $T$ is the chosen temperature and $k_B$ is the Boltzmann constant.

The obtained specific contact resistance results are shown in Figure~\ref{fig:contactresistance}. The contact resistance at the
Dirac point (undoped graphene) strongly depends on the amount of overlap, with the widest overlaps getting close to the 100~$\Omega \cdot \mu $m
value, especially in the case of high e-h puddle broadening. The values of the contact resistance at higher/lower values of the Fermi energy
({\it i.e.} doped samples) rapidly decrease below the landmark value of 100~$\Omega \cdot \mu $m. These values are represented, for carrier
concentrations calculated according to the procedure in Ref.~\cite{CarrierCapacitanceGraphene}, as a function of the graphene-graphite overlap,
$L_c$, in Figure~\ref{fig:Rc_vs_Lc}, where we see that the $R_c$ values effectively saturate for $L_c >~\sim\!20$~\AA\ in highly doped samples,
while $L_c$ might extend for a few 10s of \AA\ more when contacting lowly doped graphene.
\begin{figure}
  \centering
  \includegraphics[width=1.0\linewidth]{figure_5.pdf}
  \caption{Specific Contact Resistance as a function of contact length for
  different graphene excess carrier concentrations at 300~K.}
  \label{fig:Rc_vs_Lc}
\end{figure}
To calculate the conductance and contact resistance for doped samples it was assumed that the electrostatics of the interface remain
constant for different values of the doping. To asses this assumption, the Density of States were calculated for pristine graphene and for a
graphene sheet charged uniformly such that $E_F - E_D = 0.2$ eV. Represented in Fig.~\ref{fig:grapheneDOS}, the DOS for a charged sheet of
graphene does not change substantially, it shifts to the left for about 0.2 eV. The total charge added in a 2 atom primitive cell is
$\sim-2.52$mC.
\begin{figure}
    \centering
    \includegraphics[width=1.0\linewidth]{graphene_DOS-uncharged_charged} \\
    \caption{Graphene density of states (DOS) for pristine graphene compared with
    charged graphene such that the total charge accounts for $E_F - E_D = 0.2$ eV.}
    \label{fig:grapheneDOS}
\end{figure}

\subsection{Current path analysis}

In order to asses our conclusions and gain insight into the graphite-graphene coupling, an eigenchannel analysis has been carried out using the
Inelastica package \cite{general-methods, eigenchannels}. In Figure~\ref{fig:current}, current is represented by arrows for each atom in the
geometry, represented by translucid balls, and with the arrow thickness proportional to the magnitude of the current. Each plot corresponds
to a wave vector $k_{\perp}$, perpendicular to the plane of the representation, different energy of the incoming particle and/or different
overlap, resulting in a transmission coefficient, T.
\begin{figure}[t!]
    \centering
    \includegraphics[width=0.7\linewidth]{figure_6a.pdf} \\
    \vspace{0.5cm}
    \includegraphics[width=0.7\linewidth]{figure_6b.pdf} \\
    \vspace{0.5cm}
    \includegraphics[width=0.7\linewidth]{figure_6c.pdf} \\
    \vspace{0.5cm}
    \includegraphics[width=0.7\linewidth]{figure_6d.pdf} \\
    \vspace{0.1cm}
    \caption{
    Current paths for different contact lenghts for both carriers at fixed $k_{\perp}$ = 0.660~$\pi/a_{\perp}$.
    (a) Overlap 2 for e: E =~ 0.105~eV, T = 0.99016, (b) Overlap 5 for e: E =~ 0.105~eV, T = 0.4890, (c) Overlap
    5 for h: E = -0.105~eV, T = 0.51762, (d) Overlap 9 for e: E =~ 0.105~eV, T = 0.15161 }
    \label{fig:current}
\end{figure}

Figures~\ref{fig:current}.(a)-(d) show the current lines for electrons in the cases with overlap 2, 5 and 9, with $k_{\perp}$
and $E$ chosen in such a way that high $T$'s are obtained. We can see that, as the overlap increases, injection becomes more
distributed across the overlapping area, in opposition to the case of metal-graphene contacts~\cite{XCS}, where only 1-2
metal-carbon bonds contributed to injection. Notwithstanding that, when only a very small area is available for injection
(e.g. overlap 2), high transmission is still achievable, with nearly complete injection to graphene taking place through the last two pairs.

\section{Discussion and summary \label{sec:conclusions}}

Of course, any graphite-graphene contact will eventually need to be contacted to metal leads. One set of measurements of the
contact resistance of metal-multilayer graphene (1,3,4,$\sim$50,$\sim$100 layers) did not find any strong dependence on the
number of layers, which was attributed to only the top layer or two of a graphene stack playing a role in the contact
formation~\cite{metalmultilayer}. It is expected that a different fabrication procedure promoting the formation of edge
metal-C bonds, such as demonstrated in Ref.~\cite{NiEtchedGContacts}, would significantly decrease the metal-graphite contact resistance.

In conclusion, it was shown that graphite-graphene contacts provide a promising route towards the reduction of the contact
resistance in graphene FET channels. Although transfer lengths are significantly higher than in metal-graphene contacts,
their magnitudes are still quite small, at a few tens of \AA. In addition, edge graphite-graphene contacts are expected to
have quite lower contact resistance.
