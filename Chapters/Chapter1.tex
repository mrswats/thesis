% Chapter 1

\chapter{Introduction}

\label{introduction}

\section{A look back}
% Preambul propi de la tesi, parlant sobre quina ha estat la historia de la enginyeria electronica partint
%de la llei de moore.
Before jumping into density functional theory or the wonders of molecular dynamics, some context has to be given.
Human knowledge is very vast and it is necessary, if not mandatory, to put all things in its place otherwise it
does not make sense. One could say that the study of electronics and semiconductors exploded when J. Bardeem,
W. H. Brattain and W. Schockley invented the transistor back in 1947. Soon after, the first integrated circuit was
built as well, and from there on the development of this new branch of knowledge, semiconductor physics, or semiconductor
engineering really took off. In 1965, G. Moore published his paper~\cite{moore1965} predicting
the growth of the number of transistors in a chip would double every 18 months (See Fig.~\ref{fig:mooreslaw}).
But, since recently this is no longer the case. After the transistor gate reached the mark of ~14nm, Si Metal Oxide
Semiconductor Field Effect Transistor (MOSFET) scaling is no longer possible at previous rates due to discreteness of
the matter, short-channel effects or the deteriorating effects of parasitics. To palliate these effects researchers
have dealt with this phenomenon with new MOSFET architectures and using other materials. Nevertheless this only
delays the moment in which the MOSFET scaling becomes impractical.
\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{Moores_Law_Transistor_Count_1971-2016.png}
    \caption{Moore's Law diagram. Every 18 months the number of transistors on a chip doubles~\cite{mooreslawfigure}.}
    \label{fig:mooreslaw}
\end{figure}
In radio-frequency, electrical engineers and researchers are looking for new materials with ultra-high mobilities.
The objective is to develop transistors with mobilities operating in the terahertz gap (0.3-3 THz),
which has not been used yet. Activities in 2D materials such as graphene or molybdenum disulfide (MoS$_2$) have grown in
recent years to stay. This kind of materials allows new architectures, features and properties in MOSFETs
with respect to bulk traditional mateirials. The high mobility of a 2 Dimensional Electron Gas (2DEG) is yet to
be exploited thanks to this confinement and will be an outbreak for the following years.
%TODO: Parlar de millor control del voltatge de canal i compte pq hi ha MEMTs

\section{Materials Science}
The study of materials science has advanced to the point where it is possible to design a new material atom
by atom or layer by layer. So, it is essential to have a systematic way of classifying materials and
ordering in such a way that the systematic study of them is possible. Crystallography is the study of said materials
and the classification of materials and crystals with respect to the atom's arrangement. In this next section,
a brief introduction about the crystalline structure of materials is going to be set out, along with the energy
bands interpretation, which is going to be useful later on. Further reading in Ref.~\cite{Sze2006}.

\subsection{Crystal Structure}
All the properties of any material are closely related to the crystalline structure present at the atomic level.
For a crystalline solid, there exist three primitive vectors such that the crystal structure remains invariant
under a translation through any vector. This is called the Bravais or direct lattice,
\begin{align}
    \textbf{R} = n_1 \textbf{a}_1 + n_2 \textbf{a}_2 + n_3 \textbf{a}_3,
    \label{eq:bravais}
\end{align}
where $n_i$ are the primitive vectors of the lattice, with $i=1,2,3$ are integer numbers. The lattice is now described
by these characteristic vectors. The crystal is defined by adding a set of atoms at each point of the lattice (basis).
Ideally, these crystals would span to infinity so there is not surface or edge problems. For now, let's
consider there is an infinite crystal. Since it is infinite, there is a region in this crystal that can be defined in
a way that atoms inside this region can be used as building blocks, called a primitive cell.
One kind of primitive cell of any given lattice is the Wigner-Seitz cell. This Wigner-Seitz
cell can be constructed by drawing perpendicular bisector planes in the lattice from the chosen
point to the nearest equivalent lattice sites. In Fig.~\ref{fig:basic} the three different basic cubic structures
can be found: simple cubic, face centered cubic and body centered cubic.
\begin{figure}
    \centering
    \includegraphics[width=0.9\linewidth]{bravais_lattices.jpg}
    \caption{Three basic bravais lattices from left to right: Simple cubic, Body Centered Cubic (BCC) and Face Centered Cubic (FCC).
    Figure adapted from Ref.~\cite{AshcroftMermin}.}
    \label{fig:basic}
\end{figure}
It is very convenient, in the solid state theory, to define the reciprocal of any given lattice.

The reciprocal lattice can be defined, for a given set of vectors in~\ref{eq:bravais} as
\begin{align}
    \textbf{b}_1 = 2\pi \frac{\textbf{a}_2 \times \textbf{a}_3}{\textbf{a}_1\cdot\textbf{a}_2\times\textbf{a}_3}, ~
    \textbf{b}_2 = 2\pi \frac{\textbf{a}_3 \times \textbf{a}_1}{\textbf{a}_1\cdot\textbf{a}_2\times\textbf{a}_3}, ~
    \textbf{b}_3 = 2\pi \frac{\textbf{a}_1 \times \textbf{a}_2}{\textbf{a}_1\cdot\textbf{a}_2\times\textbf{a}_3}
\end{align}
so that $\textbf{a}_i\cdot\textbf{b}_j=2\pi\delta_{ij}$ with $i=1,2,3$ and where $\delta_{ij}$ is the Korneker delta.
The general reciprocal lattice can be written as
\begin{align}
    \textbf{k} = k_1 \textbf{b}_1 + k_2 \textbf{b}_2 + k_3 \textbf{b}_3
\end{align}
where $k_i$ are the reciprocal lattice vectors, with $i=1,2,3$ are integer numbers. Equivalently to the Wigner-Seitz cell,
in the reciprocal space the first Brillouin zone can be defined. It is a Wigner-Seitz cell in this reciprocal space.
In Fig.~\ref{fig:replat} the Brillouin zone for two of the most common crystal structures is shown for reference
\footnote{The First Brillouin zone for the cubic lattice is another cubic lattice with lattice parameter $\pi/a$.}.
\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{brillouin_zones.png}
    \caption{(a) First Brillouin zone for the body-centered lattice (b) First Brillouin zone for the face-centered lattice.
    Figure adapted from Ref.~\cite{AshcroftMermin}.}
    \label{fig:replat}
\end{figure}
This, defines the basis for the crystallography, the classification of all crystals according to its symmetry
group. The reciprocal lattice is used, among other things, to draw the energy bands with respect to the Brillouin points.

The energy band structure of a crystalline solid is the relation between the energy and the quasi momentum, $\textbf{k}$ of
the electrons within the solid. This energy dispersion is unique for every solid, so it can provide a lot of information about
the material at hand. It is usually obtained from the solution to the Schrödinger equation:
\begin{align}
    \hat{\mathcal{H}}~\psi_{\textbf{k}}(\textbf{r}) = E_{\textbf{k}}~\psi_{\textbf{k}}(\textbf{r})
\end{align}
Where $\hat{\mathcal{H}}$ is the Hamiltonian of the crystal. In order to account for the periodicity on any given
crystal, the Bloch theorem is used~\cite{Bloch}. When the potential $V(\textbf{r})$ in the Hamiltonian is periodic, the
wave function is
\begin{align}
  \psi_\textbf{k}(\textbf{r}+\textbf{R}) = e^{i\textbf{k}\cdot\textbf{R}} \psi_k(\textbf{r}),
\end{align}
Bloch's theorem allows to find the total crystal wave function that accounts for that periodicity. Each state of the crystal
can be labeled with $\textbf{k}$, which is a reciprocal vector that represents the phase shift from one site to another.
So, it is possible to obtain the bands of a crystalline solid by solving the Schrödinger equation and finding the relation
$E(\textbf{k})$. In the case of semiconductors or insulators, Fig.~\ref{fig:energy_bands} represents a generic band diagram
which two bands are repsesented, the conduction band, E$_c$ and the valence band, E$_v$.
\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{Energy_bands.png}
  \caption[General form of Energy Bands]{General form of Energy Bands for a semiconductor or an insulator.
  The upper band is the conduction band, $E_c$. The lower band, the valence band $E_v$. The distance between
  the two bands is the bandgap, $E_g$. Picture adapted from \cite{Sze2006}}
\label{fig:energy_bands}
\end{figure}
In general, two regions can be distinguished. The Conduction band, upper band, labelled $E_c$. The Valence band, the lower band,
labelled $E_v$. The energy bandgap is defined as
\begin{align}
  E_g = \vert \text{min}(E_c) - \text{max}(E_v) \vert.
\end{align}
This value is one of the most important parameters in material physics because materials can be classified as insulators, semiconductors,
semimetallic or metal depending on this value, E$_g$. Another relevant quantity in semiconductor physics is the Fermi level
\footnote{Strictly speaking, the chemical potential.}. The Fermi level, $E_f$, is the energy level corresponding to the last
occupied state in the band diagram at 0K. At a finite temperature, is the average change in energy when a particle is added
to the system. In a semiconductor material it is possible to find two types of charge carriers electrons and holes. The position
of the Fermi level determines the majority carriers in a semicondcutor. If the majority carriers are electrons, the semiconductor
is often called a $n$-type semiconductor. If the majority carriers are holes, it is otherwise called a $p$-type semiconductor.

\subsection{2D Materials}
In this thesis a focus has been put in 2-Dimensional (2D) materials. This kind of materials is characterized by the fact
that they are atomically narrow in one direction while being of arbitrary size along the other two. This causes a quantization
of the energy levels for the electrons creating the so called 2 dimensional electron gas (2DEG), which has very interesting properties
from the physical point of view. One of the most interesting and relevant properties that are useful in electronics is the
high mobility of the electrons when they are confined.

Carbon allotropes have been observed before in different forms and dimensionalities. From the C$_{60}$ ``bucky ball''~\cite{buckyball}
to the carbon nano tubes (CNT)~\cite{CNTs} that correspond to quasi 0D and quasi 1D systems. And being graphite the 3D counterpart,
a free-standing allotrope for a 2D carbon had not been yet observed. It was not until Novoselov, Geim \textit{et. al.}~\cite{Novoselov1, Novoselov2, Novoselov3}
reported the observation and measurements of graphene, a single-layer graphite. In a way, graphene is the unfolding of a CNT.
This opened the door to the study of several other 2D materials using similar techniques~\cites{vdwheterostructures}.
With a collection of 2D van der Waals layered materials it is possible to create heterostructures with different functionalities such
as LEDs~\cite{heteroLED}, atomically thin $p-n$ junctions~\cite{vdwpnjunction}, photodetectors~\cite{graphenephotodetectors} and other devices~\cite{vdwhsdevices}.

Although graphene is not much of use for digital logic applications, due to the lack of a band gap, graphene field effect transistors (FETs) have been fabricated.
It has been theorized that the ultra-high electron mobility in graphene can be used to bridge the terahertz gap in high
frequency applications~\cite{grapheneroadmap, grapheneterahertz}. The objective of the present thesis has been in the long range goal of High-Frequency
electronics. Over the course of this work, 2D materials and low dimensionality structures have been used to this end and they have been studied with such
applications in mind. These materials are described in the following sections: graphene, molybdenum disulfide, hafnium oxide and graphite,
to name a few.

\subsubsection*{Graphene}
Graphene is a 2-dimensional material formed by carbon atoms disposed in a honeycomb lattice. It is known for conducing both
electricity and heat very efficiently and is structurally strong.
\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{graphene}
    \caption{Graphene Honeycomb Lattice. Lattice parameter $a = 2.4829$ \AA. Picture generated with VMD~\cite{VMD}.}
    \label{fig:graphenestr}
\end{figure}
From the state crystallographic point of view this material can be described as a hexagonal lattice
structure with a basis of two atoms. The primitive vectors can be taken as
\begin{align}
    \nonumber
    \textbf{a$_1$} = a \bigg(1,0\bigg), ~
    \textbf{a$_2$} = a \bigg(\frac{1}{2},\frac{\sqrt{3}}{2}\bigg)
 \end{align}
where $a$ is the lattice constant. The atomic positions (in fractional coordinates of the primitive vectors) are given by
\begin{align}
    \nonumber
    \textbf{b'$_1$} = \bigg(-\frac{1}{6},+\frac{1}{3}\bigg), ~
    \textbf{b'$_2$} = \bigg(+\frac{1}{6},-\frac{1}{3}\bigg).
\end{align}

\subsubsection*{Graphite}
Graphite is a mineral composed solely by carbon. In particular, it can be thought of as "bulk" graphene. Its crystalline structure
is graphene with an AB stacking, see Fig.~\ref{fig:graphtiestr}. The different layers are bound by van der Waals forces.
\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{graphite}
    \caption{Graphite AB stacking. Each graphite layer is a graphene layer shifted with respect to the adjacent layers.
    Lattice parameters $a=2.488$\AA, $c=6.54$\AA. Top view (left), side view (right).
    Picture generated with VMD~\cite{VMD}.}
    \label{fig:graphtiestr}
\end{figure}
Its crystallographic characteristics are very similar to graphene, but with an extra layer.
\begin{align}
    \nonumber
    \textbf{a$_1$} = a \bigg(\frac{1}{2}, +\frac{\sqrt{3}}{2}, 0 \bigg), ~
    \textbf{a$_2$} = a \bigg(\frac{1}{2}, -\frac{\sqrt{3}}{2}, 0 \bigg), ~
    \textbf{a$_3$} = c \bigg(0, 0, 1 \bigg),
\end{align}
where $a$ is the in-plane lattice constant and $c$ the vertical lattice. The atomic positions with respect to the lattice vectors are
\begin{align}
    \nonumber
    \textbf{b'$_1$} = \bigg(0, 0, 0                                 \bigg), ~
    \textbf{b'$_2$} = \bigg(-\frac{1}{3}, +\frac{1}{6}, 0           \bigg), ~
    \textbf{b'$_1$} = \bigg(0, 0, \frac{1}{2}                       \bigg), ~
    \textbf{b'$_4$} = \bigg(+\frac{1}{3}, -\frac{1}{6}, \frac{1}{2} \bigg).
\end{align}

\subsubsection*{Molybdenum disulfide (MoS$_2$)}
Bulk Molybdenum disulfide (MoS$_2$) is a layered semiconductor with an indirect band gap~\cite{bulkmos2}. In its monolayer form,
the band structure changes to a direct gap semiconductor with a band gap of ~$1.8$ eV~\cite{layeredmos2}. The fact that this
material is a semiconductor is useful in the sense that it is possible to investigate further in 2D materials with parabolic bands
(see Sec.~\ref{ssec:graphenevstheworld}). The different layers are held by van der Waals forces, can, like graphene, be
obtained via mechanical exfoliaton~\cite{exfoliatedmos2}.
\begin{figure}
    \centering
    \includegraphics[width=0.75\linewidth]{mos2_structure}
    \caption{MoS$_2$ mono layer top view (left) and side view (right). Molybdenum atoms are represented by Pink balls and Sulfur atoms are
    represented by yellow balls. Image generated with VMD~\cite{VMD}.}
    \label{fig:mos2str}
\end{figure}
From a crystallographic point of view, a mono layer MoS$_2$ can be described as an hexagonal structure
\begin{align}
    \nonumber
    \textbf{a$_1$} = a \bigg(\frac{1}{2},+\frac{\sqrt{3}}{2}\bigg), ~
    \textbf{a$_2$} = a \bigg(\frac{1}{2},-\frac{\sqrt{3}}{2}\bigg),
\end{align}
where $a$ is the in-plane lattice constant. The atomic positions, in fractional coordinates with respect to the
lattice vectors in the primitive cell, are given by
\begin{align}
    \nonumber
    \textbf{b'$_1$} = \bigg(0, 0\bigg), ~
    \textbf{b'$_2$} = \bigg(\frac{2}{3},\frac{1}{3}\bigg).
\end{align}

\subsection{Other materials}
\subsubsection*{Hafnium Oxide (HfO$_2$)}
Hafnium Oxide is a high-$\kappa$ insulator with a wide band gap of around~$5.3-5.7$ eV~\cite{hafniumoxide}. Its dielectric constant
is around $25~\varepsilon_0$~\cite{highkoxides}, which makes it a very strong candidate for thin layer insulator in nanoelectronic
devices. Its crystalline structure has different phases being the monoclinic and amorphous phases the most common. It is also used
in resistive switching technologies due to its composition and crystalline structure~\cite{resistiveswitching}.

\subsection{Graphene vs. traditional semiconductors \label{ssec:graphenevstheworld}}
Graphene is a very particular material for many reasons. First, it is a 2D material meaning that the dimensionality
on one direction is very small. Second, is made entirely out of carbon, which is not an element usually found in the semiconductor
industry, but as an impurity. Finally, the band diagram of graphene is very particular in the sense that it
is neither a semiconductor or an insulator, but a semimetal as the bands cross at the $K$ point in the Brillouin zone. Near this
point the bands of graphene can be approximated by Eq.~\ref{eq:graphenebands}.
\begin{align}
    E(k) = v_F~\hbar~\vert k \vert
    \label{eq:graphenebands}
\end{align}
where $v_F$ is the Fermi velocity which takes values $\sim10^6~m/s$. On the other hand, near the band edge the conduction band of traditional
semiconductors can be approximated by Eq.~\ref{eq:scbands}, which is, instead, parabolic in $k$.
\begin{align}
    E(k) = \frac{h^2 k^2}{2m}
    \label{eq:scbands}
\end{align}
 Figure \ref{fig:bands} shows the band diagram for graphene and silicon is compared. As mentioned before, graphene bands cross at the
 $K$ point of the Brillouin zone while Silicon bands do not cross and feature an experimental gap of ~$1.11$ eV at 300K~\cite{kittel}.
\begin{figure}
    \centering
    \includegraphics[width=0.49\linewidth]{silicon_bulk_bands}
    \includegraphics[width=0.49\linewidth]{graphene_bands}
    \captionof{figure}{Energy bands for Silicon (left) and Graphene (right). The green line represents the Fermi level
    for intrinsic concentrations of carreirs.}
    \label{fig:bands}
\end{figure}
In digital electronics, the band gap is an essential magnitude to build, for example, a transistor.
This allows the transistor to be switched on and off. However, graphene cannot be used for digital
applications as per not having a band gap. Instead, it can be used for analog applications~\cite{graphenerfapps},
its high mobility~\cite{highmobilitygraphene} or its high heat conductivity may be better suited for other uses.

%Section on RF electronics maybe? Or something of the like more generitc to electronic engineering

\section{Outline}
This thesis is structured in the following way. In Chapter \ref{introduction} a
brief introduction to semiconductors and 2D materials with an emphasis on Graphene has been made.
Following this, in Chapter \ref{methods} the theoretical background for the different
approaches will be described as well as some details on the implementation uesd.
Then, each of these techniques will be put to use and show some studies on different
2DM-based analog FETs and its results. In Chapter \ref{driftdifusion} a 2D based FET is studied
through the finite element method to obtain a macroscopic model with a set of parameters to model
MoS$_2$ within this method. Afterwards, in Chapter \ref{moleculardynamics}
a parametrisation for carbon nickel and palladium in the BOP frame was found,
discussed and tested for the simulation of metal deposition on a graphene substrate to study the
contact resistence of a metal-graphene contact. In Chapter \ref{ggraphite} the \emph{ab initio} study of contact
resistance between graphite and graphene and then using graphite as a buffer for the contact
for the obtention of a contact structure that is suitable for high Rf transistors.
The NEGF equations are used, along with DFT, to calculate ballistic transport through the studied
geometry and finally compute the contact resistance from this system. Finally, in Chapter \ref{globalconclusions},
a summary of the conclusions of this thesis.
% Additionally, Appendix \ref{} the full derivation of the Landauer
% formula is shown and in Appendix \ref{} all the geometries used for the PTMC algorithm in Chapter \ref{moleculardynamics}
