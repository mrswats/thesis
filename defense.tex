%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beamer Presentation
% LaTeX Template
% Version 1.0 (10/11/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------

\documentclass[10pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}

\mode<presentation> {

% The Beamer class comes with a number of default slide themes
% which change the colors and layouts of slides. Below this is a list
% of all the themes, uncomment each in turn to see what they look like.

%Font theme
\usefonttheme{serif}

\usetheme{default}
% \usetheme{AnnArbor}
% \usetheme{Antibes}
% \usetheme{Bergen}
% \usetheme{Berkeley}
% \usetheme{Berlin}
% \usetheme{Boadilla} %***
\usetheme{CambridgeUS}
% \usetheme{Copenhagen}
% \usetheme{Darmstadt}
% \usetheme{Dresden}
% \usetheme{Frankfurt}
% \usetheme{Goettingen}
% \usetheme{Hannover}
% \usetheme{Ilmenau}
% \usetheme{JuanLesPins}
% \usetheme{Luebeck}
% \usetheme{Madrid}
% \usetheme{Malmoe}
% \usetheme{Marburg}
% \usetheme{Montpellier}
% \usetheme{PaloAlto}
% \usetheme{Pittsburgh}
% \usetheme{Rochester}
% \usetheme{Singapore}
% \usetheme{Szeged}
% \usetheme{Warsaw}

% As well as themes, the Beamer class has a number of color themes
% for any slide theme. Uncomment each of these in turn to see how it
% changes the colors of your current slide theme.

% \usecolortheme{albatross}
\usecolortheme{beaver}
% \usecolortheme{beetle}
% \usecolortheme{crane}
% \usecolortheme{dolphin}
% \usecolortheme{dove}
% \usecolortheme{fly}
% \usecolortheme{lily}
% \usecolortheme{orchid}
% \usecolortheme{rose}
% \usecolortheme{seagull}
% \usecolortheme{seahorse}
% \usecolortheme{whale}
% \usecolortheme{wolverine}

%\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
%\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line
\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}

\usepackage{graphicx} % Allows including images
\graphicspath{{Figures/}{./}} % Specifies where to look for included images

\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables

\newcommand{\diff}{\text{d}}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

% The short title appears at the bottom of every slide, the full title is only on the title page
\title[2DFETs]{Contact Resistance and electrostatics of 2DFETs}

\author{Ferran Jovell Megias} % Your name
\institute[UAB] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
Universitat Autònoma de Barcelona \\ % Your institution for the title page
\bigskip
% \textit{Ferran.Jovell@uab.cat} % Your email address
{\footnotesize Thesis defense for}
\newline
\textit{PhD in Electrical and Telecommunication Engineering}
}
\date{July 16th, 2018} % Date, can be changed to a custom date

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSubsection[]
% {
%   \begin{frame}<beamer>{Outline}
%     \tableofcontents[currentsection,currentsubsection]
%   \end{frame}
% }

\AtBeginSection[]
{
    \begin{frame}<beamer>{Outline}
        \tableofcontents[currentsection]
    \end{frame}
}

\begin{document}

\begin{frame}
    \titlepage % Print the title page as the first slide
    \begin{center}
    \footnotesize
    {Advisor: Xavier Cartoix\`a}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Outline} % Table of contents slide, comment this block out to remove it
    \tableofcontents
    % Throughout your presentation, if you choose to use \section{} and \subsection{} commands,
    %these will automatically be printed on this slide as an overview of your presentation
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\section{Introduction}

\begin{frame}
    \frametitle{High-Frequency Devices}
    \textbf{What are High-Frequency analog devices?}
    \begin{itemize}
        \item<1-> \textit{Transistors}, \textit{amplifiers}, \textit{signal mixers}, \textit{oscillators}, etc$\dots$
        \item<2-> Nowadays the frequency of operation: $\sim$ 1-100 GHz range.
        \begin{center}
            \includegraphics[width=0.4\linewidth]{gfet_performance}
        \end{center}
        \item<3-> \textbf{Goals}: Improve performance by reducing power, materials with high mobility.
        \item<4-> 2DFETs are very good candidates for HF applications.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Graphene and 2D materials}
    \begin{onlyenv}<-3>
            \begin{itemize}
                \item<1-> With the breakthrough of Geim, Novoselov \textit{et. al.} graphene and other \textbf{2D materials}
                started to be heavily researched.
                \item<2-> A new class of materials offered new possibilities.
                \item<3-> 2D materials include graphene, MoS$_2$, h-BN, WS$_2$, etc$\dots$
                \begin{center}
                    \includegraphics<3->[width=0.49\linewidth]{graphene}
                    \includegraphics<3->[width=0.49\linewidth]{mos2_structure-2}
                \end{center}
            \end{itemize}
    \end{onlyenv}
    \begin{onlyenv}<4->
        \begin{block}{Stands out$\dots$}
            The characteristics of graphene make it an \textbf{outstanding} candidate for many applications.
            \begin{center}
                \includegraphics[width=0.49\linewidth]{mos2_bands}
                \includegraphics[width=0.49\linewidth]{graphene_bands}
            \end{center}
            Ultra-high mobility, high heat conduction and flat bands near the $K$ point.
        \end{block}
    \end{onlyenv}
\end{frame}

\begin{frame}
    \frametitle{Computational Methods}
    \textbf{Computational methods} are tools for prediction and study. Also, they sometimes allow for the
    performance of (numerical) experiments under very controlled environments.
    \begin{center}
        \includegraphics<1>[width=0.66\linewidth]{simulation_size-vs-simulation_time.pdf}
        \includegraphics<2>[width=0.66\linewidth]{simulation_size-vs-simulation_time-2.pdf}
        \includegraphics<3>[width=0.66\linewidth]{simulation_size-vs-simulation_time-3.pdf}
        \includegraphics<4>[width=0.66\linewidth]{simulation_size-vs-simulation_time-4.pdf}
    \end{center}
\end{frame}

% There should be *something* else in here, I think

%------------------------------------------------

\section{MoS$_2$ devices}
\subsection{Introduction}
\begin{frame}
    \frametitle{Motivation and Goals}
    \begin{itemize}
        \item<1-> Learn and understand \textbf{Drift-Diffusion simulations} using the Finite Element Method.
        \item<2-> Simulate 2D channel based FETs (2DFETs) within this framework.
        \item<3-> With the MoS$_2$ parametrization in this model, study and compute the depletion
        lengths of a $p-n$ MoS$_2$ junction.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The Drift-Diffusion model}
    The Drift-Diffusion \textbf{model} is described by five equations.
    \begin{block}{Drift-Diffusion Equations}
        \begin{align}
            \nabla \cdot \left[ \varepsilon\,\nabla\phi \right] &= - \rho(\textbf{r}) \\
            \nabla \cdot J_n &= -q \left( G_n - R_n \right) \\
            \nabla \cdot J_p &= +q \left( G_p - R_p \right) \\
            J_n &= qn\mu_n \nabla\phi + q D_n \nabla n \\
            J_p &= qp\mu_p \nabla\phi - q D_p \nabla p
        \end{align}
    \end{block}
\end{frame}

\begin{frame}
        \frametitle{The Finite Element Method}
        \begin{itemize}
            \item<1-> \textbf{Numerical scheme} to solve a set of differential equations with boundary conditions.
            \item<2-> This method allow arbitrary geometries through the use of a mesh.
            \item<3-> The Garlekin Weighted residual method uses tent functions at each point in the mesh.
            \begin{center}
                \includegraphics[width=0.49\linewidth]{mesh_metal-mos2_interface}
                \includegraphics[width=0.49\linewidth]{Finite_element_method_1D_illustration2}
            \end{center}
        \end{itemize}
\end{frame}

\subsection{Single-layer MoS$_2$ MOSFET}
\begin{frame}
    \frametitle{Geometry}
    The geometry studied is a traditional MOSFET geometry with a \textbf{MoS$_2$ 2D channel} with gold contacts.
    \begin{center}
        \includegraphics[width=0.66\linewidth]{exp_mos2_transistor}
    \end{center}
    {\footnotesize Radisavljevic \textit{et.al.}, Nat. Nanotech. 6, p. 147–150 (2011)}
\end{frame}

\begin{frame}
    \frametitle{Material parameters}
    \begin{table}
        \centering
        \begin{tabular}{ |c|c|c|c| } \hline
            Parameter & MoS$_2$\\ \hline
            $\varepsilon$ & 4$~\varepsilon_0$ \\
            $\chi$  & 6.0 eV \\
            E$_g$   & 1.9 eV \\
            $m^*_e$ & 0.54 $m_e$ \\
            $m^*_h$ & 0.44 $m_e$ \\
            $\mu_n$ & 217 cm$^2 V^{-1} s^{-1}$ \\
            $\mu_p$ & 40  cm$^2 V^{-1} s^{-1}$ \\ \hline
        \end{tabular}
        % \caption{Drift-diffusion parameters for MoS$_2$. Permitivitty $\varepsilon$, Electronic Affinity $\chi$,
        % Energy Band Gap $E_g$ (300K), Effective mass for electrons and holes $m^*_{e,h}$ and mobility for electrones and holes $\mu_{e,h}$.}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{I-V characteristics}
    Output characteristics of the simulated Single-Layer MoS$_2$ transistor.
    \begin{center}
        \includegraphics[width=0.66\linewidth]{Ids_vs_Vg_mos2mosfet}
    \end{center}
    {\footnotesize Radisavljevic \textit{et.al.}, Nat. Nanotech. 6, p. 147–150 (2011)}
\end{frame}


\subsection{2D MoS$_2~p-n$ junction}
\begin{frame}
    \frametitle{MoS$_2$ $p-n$ junction}
    \begin{overprint}
        \onslide<1> $p-n$ junction geometry.
        \onslide<2> Charge concentration and potential.
        \onslide<3> Contour potential.
    \end{overprint}
    \begin{center}
        \includegraphics<1>[width=0.49\linewidth]{mos2_junction_diagram}
        \includegraphics<2>[width=0.49\linewidth]{mos2_junction_chargeconc_2}
        \includegraphics<2>[width=0.49\linewidth]{mos2_junction_potential}
        \includegraphics<3>[width=0.49\linewidth]{mos2_junction_contourpotential_1}
    \end{center}
    \begin{overprint}
        \onslide<1> MoS$_2$ channel (purple), Gold contacts (Blue), Vacuum (Yellow)
        \onslide<2> {\footnotesize
        $1.0\times10^{10}$ cm$^{-2}$ (red), $5.0\times10^{10}$ cm$^{-2}$ (light blue)
        \newline
        $1.0\times10^{11}$ cm$^{-2}$ (green), $5.0\times10^{11}$ cm$^{-2}$ (yellow)
        \newline
        $1.0\times10^{12}$ cm$^{-2}$ (dark blue)}
    \end{overprint}
\end{frame}

\begin{frame}
    \frametitle{Results}
    \begin{onlyenv}<1>
        \begin{block}{Depletion widths for the MoS$_2$ $p-n$ junction}
            \begin{table}
                \centering
                \begin{tabular}{ |c|ccccc| } \hline
                    W $\backslash$ x$_p$ & $1.0\times10^{10}$ & $5.0\times10^{10}$ & $1.0\times10^{11}$ & $5.0\times10^{11}$ & $1.0\times10^{12}$ \\ \hline
                    0.1 $\mu$m    &        &        &       & 10 nm & 10 nm \\ \hline
                    1 $\mu$m      &        &        & 50 nm &       &       \\ \hline
                    10 $\mu$m     & 400 nm & 100 nm & 50 nm & 14 nm & 9 nm  \\ \hline
                    10 $\mu$m$^*$ & 600 nm &        &       &       &       \\ \hline
                    100 $\mu$m    & 600 nm &        &       &       &       \\ \hline
                \end{tabular}
            \end{table}
            Depletion zone as a function of the Channel Width (W) at different doping levels (cm$^{-2}$) with Gold
            \textbf{Shottky} contacts. The blank spaces left represent flat depletion zone. $^*$This corresponds to a
            simulation with Gold Ohmic contacts instead.
        \end{block}
    \end{onlyenv}
    \begin{onlyenv}<2>
        \begin{center}
            \includegraphics[width=0.8\linewidth]{xp_vs_n}
        \end{center}
    \end{onlyenv}
\end{frame}

\subsection{Conclusions}
\begin{frame}
    \frametitle{Conclusions}
    \begin{itemize}
        \item<1-> A \textbf{single-layer} MoS$_2$ MOSFET and a MoS$_2$ $p-n$ junction were studied using the Drift-Diffusion model.
        \item<2-> The \textbf{qualitative} behaviour of the Single-Layer MoS$_2$ MOSFET can be reproduced with this model.
        \item<3-> In order to reproduce the experimental data exactly, some \textbf{modifications} on the paraeters or
        the geometry would be needed.
        \item<4-> The dependence of the space charge zone width on the dopant concentration is \textbf{stronger} in 2DMs.
        \item<5-> The region with curved bands has a different extent than the region with non-zero charge density.
    \end{itemize}
\end{frame}

%------------------------------------------------

\section{Contact Resistance in graphite-graphene contacts}
\subsection{Introduction}
\begin{frame}
    \frametitle{Introduction}
    \begin{onlyenv}<1-2>
        The contact resistance at the interface between an electrode and the channel is key.
        \begin{block}{Figures of merit}
            These quantities asses the \textbf{performance} of a HF device. $f_T$ is the cutoff frequency (unity current
            gain) and $f_{max}$ the maximum frequency of operation (unity power gain). ({\footnotesize Schwierz, Proc.
            IEEE 101, 1567 (2013)})
            \pause
            \begin{align}
                f_T =& \frac{g_m}{2\pi(C_{gs}+C_{gd})} \frac{1}{1+g_{ds}(R_S+R_D)+\frac{C_{gd}g_m(R_S+R_D)}{C_{gs}+C_{gd}}} \\
                f_{max} =& \frac{g_m}{4\pi C_{gs}} \frac{1}{ \left( g_{ds} (R_i+R_S+R_G) + g_m R_G \frac{G_{gd}}{C_{gs}} \right)^{\frac{1}{2}} }
            \end{align}
        \end{block}
    \end{onlyenv}

    \begin{onlyenv}<3->
        \begin{align} \nonumber
            f_T =& \frac{g_m}{2\pi(C_{gs}+C_{gd})} \frac{1}{1+g_{ds}(R_S+R_D)+\frac{C_{gd}g_m(R_S+R_D)}{C_{gs}+C_{gd}}} \\ \nonumber
            f_{max} =& \frac{g_m}{4\pi C_{gs}} \frac{1}{ \left( g_{ds} (R_i+R_S+R_G) + g_m R_G \frac{G_{gd}}{C_{gs}} \right)^{\frac{1}{2}} }
        \end{align}
        \begin{center}
            \includegraphics[width=0.49\linewidth]{GFET_electric_model}
            \includegraphics[width=0.49\linewidth]{GFET_geometry}
        \end{center}
    \end{onlyenv}
    \begin{overprint}
        \onslide<5-> \textbf{The consensus is that $R_c \le 100\,\Omega\cdot\mu$}m.
    \end{overprint}
\end{frame}

\begin{frame}
    \frametitle{Contact Resistance}
    \begin{onlyenv}<1-2>
        By definition, the contact resistance through an interface will be the difference between the resistance
        in two calculations, \textit{i.e.}
        \begin{block}<2>{Definition}
            \begin{align}
                R_c(E_{F}) = G^{-1}_{gg}(E_{F}) - G^{-1}_{g}(E_{F})
            \end{align}
            where $E_F$ is the Fermi level and $G^{-1}_{gg}$ refers to the resitance of the whole
            graphite-graphene structure, while $G^{-1}_{g}$ refers to the resitance of the
            graphene region.
        \end{block}
    \end{onlyenv}
    \begin{onlyenv}<3->
        In order to account for the \textbf{temperature} effects and the electron-hole \textbf{puddles}, a broadening of the
        computed conductances must be carried out leading to
        \begin{block}<4->{Definition}
            \begin{flalign}
                &R_c(E_F) = \nonumber \\
                \nonumber
                &+k_B T \left( \int \mkern-12mu \int \frac{\exp{[(E-E')/k_B T]}}{1+\exp{[(E-E')/k_B T]^2}}
                G_{gg}(E)~w(E'-E_F;\eta) \textrm{d}E \textrm{d}E' \right)^{-1} \\
                \nonumber
                &-k_B T \left( \int \mkern-12mu \int \frac{\exp{[(E-E')/k_B T]}}{1+\exp{[(E-E')/k_B T]^2}}
                G_{g~}(E)~w(E'-E_F;\eta) \textrm{d}E \textrm{d}E' \right)^{-1}
            \end{flalign}
            The temperature broadening is taken into account with the first term in the integral. The electron-hole
            puddle is considered with a Gaussian broadening included in $w(E'-E_F;\eta)$.
        \end{block}
    \end{onlyenv}
\end{frame}

\begin{frame}
    \frametitle{Transmission and Conductance}
    The relation between the quantum conductance and the \textbf{transmission} coefficient is obtained through the Landauer Formula.
    \begin{block}{The Landauer Formula}
        \begin{align}
            G(\varepsilon) =  \frac{e^2}{h}~\sum_{i=1}^{M(\varepsilon)} T_i(\varepsilon)
        \end{align}
        where $e$ is the electron charge, $h$ the Planck constant, $T(\varepsilon)$ the transmission coefficient as a function
        of the energy and $M(\varepsilon)$ the number of spin polarized modes at energy $\varepsilon$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Non-Equilibrium Green's Function Theory}
    \begin{onlyenv}<1-2>
        \begin{block}<2>{Operators}
            From the single particle Hamiltonian, the Green's \textbf{Operator} is defined along with the self energies
            and the spectral density operator.
            \begin{align}
                \hat{G}^\pm(\varepsilon) \equiv& \lim_{\eta \rightarrow 0^+} \left( \hat{\mathcal{H}} - \varepsilon \pm i \eta \right)^{-1} \\
                \Sigma_e(\varepsilon) \equiv& \left[ \hat{V} \hat{\textit{g}}^e(\varepsilon) \hat{V}^\dagger \right] \\
                \Gamma_e(\epsilon) \equiv& \frac{i}{2} \left[ \Sigma_e(\varepsilon) - \Sigma_e^\dagger(\varepsilon) \right] \\
                \mathcal{A}^e_{\mu\nu} =& \frac{1}{\pi} \left[ \hat{G}(\varepsilon) \Gamma_e(\varepsilon) \hat{G}^\dagger(\varepsilon) \right]_{\mu\nu}
            \end{align}
        \end{block}
    \end{onlyenv}
    \begin{onlyenv}<3->
        \begin{block}{Density Matrix}
            The contribution to the \textbf{density matrix} from one of the electrodes can be written as
            \begin{align}
                \hat{\rho} = -\frac{1}{\pi}\text{Im}\left[ \int_{-\infty}^{+\infty}
                \hat{G}(\varepsilon)~n_F(\varepsilon-\mu)~\diff\varepsilon \right]
            \end{align}
            in terms of the quantities in the previous slide. This integral is evaluated by contour integration in the complex plane.
        \end{block}
        \begin{block}<4->{Transmission Coefficients}
            The transmission coefficients are calculated through the \textbf{scattering matrix}.
            \begin{align}
                s_{e e'}(\varepsilon) &= - \delta_{e e'} + i [\Gamma_e(\varepsilon)]^{\frac{1}{2}}\,\hat{G}(\varepsilon)\,[\Gamma_{e'}(\varepsilon)]^{\frac{1}{2}} \\
                T_{e e'}(\varepsilon) &= \text{Tr}\left[ s^\dagger_{e e'}(\varepsilon) s_{e e'}(\varepsilon) \right]
            \end{align}
        \end{block}
        {\footnotesize Papior \textit{et. al.}, Comp. Phys. Comm., 212, p.8-24 (2017)}
    \end{onlyenv}
\end{frame}

\begin{frame}
    \frametitle{But first...}
    \textbf{What have been previously done?}
    \begin{onlyenv}<1-4>
        From the literature you find there are two types of contacts:
        \begin{itemize}
            \item<2-> Weakly bound: Pt, Ir, Al
            \item<2-> Larger metal-graphene separation ($\sim$3.7\AA)
            \item<2-> Phonon modes simlar to bulk graphite
        \end{itemize}
        \begin{itemize}
            \item<3-> Strongly bound: Ni, Ru, Pd
            \item<3-> Smaller metal-graphene separation ($\sim$2.1\AA)
            \item<3-> Out-of-plane phonon different than bulk graphite
        \end{itemize}
        \begin{itemize}
            \item<4-> Good hybridization between graphene and Ni, Ru and Pd orbitals
        %     \item<7-> \textbf{But}: There is no dependence in the amount of overlap: it is a perimeter effect.
        \end{itemize}
    \end{onlyenv}
\end{frame}

\begin{frame}
    \frametitle{Contact Geometry}
    \begin{onlyenv}<1-2>
        The graphite-graphene contact has a \textbf{top contact} geometry with a certain overlap.
        \begin{center}
            \includegraphics<1>[width=0.9\linewidth]{finite_overlap}
            \includegraphics<2>[width=0.9\linewidth]{infinite_overlap}
        \end{center}
    \end{onlyenv}
    \begin{onlyenv}<3>
        TranSiesta transport setup consists of two basic regions: Electrode(s) and scattering zone.
        \begin{center}
          \includegraphics[width=1.0\linewidth]{figure_1.pdf}
        \end{center}
    \end{onlyenv}
    \begin{onlyenv}<4->
        \textbf{Technical issues.}
        \newline
        We verify that the \textbf{interlayer} hopping is not affected by the passage from vdW to GGA
        functionals (same structure in both cases).
        \pause
        \begin{center}
          \includegraphics[width=0.66\linewidth]{figure_2.pdf}
        \end{center}
    \end{onlyenv}
\end{frame}


\begin{frame}
    \frametitle{Metal-Graphene contacts}
    \begin{onlyenv}<1->
        The \textbf{metal-graphene} contacts have already been studied.
        \begin{center}
            \includegraphics<1>[width=0.66\linewidth]{GonMe_Pd.png}
            \includegraphics<2>[width=0.66\linewidth]{GonMe_NiUP.png}
            \includegraphics<3>[width=0.66\linewidth]{GonMe_Rc-NiAlPd.png}
        \end{center}
        \begin{overprint}
            \onslide<1> \textbf{Palladium} conductance
            \onslide<2> \textbf{Nickel} (up) conductance
            \onslide<3> Specific contact resistance
        \end{overprint}
        {\footnotesize Cartoix\`a \textit{et. al.}, Metal-graphene contacts \textit{In preparation}}
    \end{onlyenv}
\end{frame}

\subsection{Graphite-graphene contacts}
\begin{frame}
    \frametitle{The perfect graphene contact}
    Of course, the best graphene contact out there is...
    \begin{center}
        \includegraphics<1>[width=0.6\linewidth]{top_graphene_view-covered}
        \includegraphics<2>[width=0.6\linewidth]{top_graphene_view}
    \end{center}
    \begin{overprint}
        \onslide<2> \hfill another \textbf{graphene} contact.
    \end{overprint}
\end{frame}

\begin{frame}
    \frametitle{Results}
    \begin{onlyenv}<1>
        Specific Conductance for different \textbf{overlap}.
        \begin{center}
          \includegraphics[width=0.66\linewidth]{figure_3.pdf}
        \end{center}
        Specific Conductance of the Graphite-Graphene contacts per unit of lattice length.
        \newline
        {\footnotesize Jovell, Cartoix\`a (\textit{In press}).}
    \end{onlyenv}
    \begin{onlyenv}<2>
        Specific contact resistance at 300K for different electron-hole puddles broadening.
        \begin{center}
            \includegraphics<2>[width=1.0\linewidth]{figure_4-2.pdf}
            \begin{enumerate}
                \item[(a)]<2> Thermal broadening at 300~K plus electron-hole puddle of 50~meV.
                \item[(b)]<2> Thermal broadening at 300~K plus electron-hole puddle at 5~meV.
            \end{enumerate}
        \end{center}
        {\footnotesize Martin \textit{et. al.}, Nature Phys. 4, 144 (2008)}
    \end{onlyenv}
    \begin{onlyenv}<3->
        Contact resistance as a function of the contact length at different graphene dopings
        \begin{center}
            \includegraphics<4>[width=0.66\linewidth]{figure_5.pdf}
        \end{center}
    \end{onlyenv}
\end{frame}

\begin{frame}
    \frametitle{Result Analysis}
    \begin{onlyenv}<1-2>
        Injection mechanism: \textbf{Area} effect, similar to a toll stop.
        \pause
        \begin{center}
            \includegraphics[width=0.66\linewidth]{embut_peatge}
        \end{center}
    \end{onlyenv}
    \begin{onlyenv}<3->
        Current path analysis reveals$\dots$
        \begin{center}
            \includegraphics<3->[width=0.49\linewidth]{figure_6a.pdf}
            \includegraphics<3->[width=0.49\linewidth]{figure_6b.pdf}
            % \caption<1>{
            % Current paths for different contact lenghts for both carriers at fixed $k_{\perp}$ = 0.660~$\pi/a_{\perp}$.
            % {\small (a) Overlap 2 for e: E =~ 0.105~eV, T = 0.99016, (b) Overlap 5 for e: E =~ 0.105~eV, T = 0.4890}
        \end{center}
        \hspace{0.2cm}
        \begin{center}
            \includegraphics<4>[width=0.49\linewidth]{figure_6c.pdf}
            \includegraphics<4>[width=0.49\linewidth]{figure_6d.pdf}
            % \caption<2>{
            % Current paths for different contact lenghts for both carriers at fixed $k_{\perp}$ = 0.660~$\pi/a_{\perp}$.
            % {\small (c) Overlap 5 for h: E = -0.105~eV, T = 0.51762, (d) Overlap 9 for e: E =~ 0.105~eV, T = 0.15161}
        \end{center}
    \end{onlyenv}
\end{frame}

\subsection{Conclusions}
\begin{frame}
    \frametitle{Conclusions}
    \begin{itemize}
        \item<1-> Graphite-graphene contacts provide a promising route towards R$_c$ \textbf{reduction}.
        \item<2-> Transfer lengths are \textbf{higher} ($\sim$ 20\AA)than in metal-graphene contacts ($\sim$ 2, 3 nm).
        \item<3-> \textbf{Injection} has a little more of an area effect, but still low $L_t$.
        \item<4-> \textbf{Edge} contacts yield low R$_c$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Conclusions, but$\dots$}
    \begin{onlyenv}<1>
        Experimental results show a \textbf{much larger} transfer length.
        \begin{center}
            \includegraphics[width=0.66\linewidth]{exp_graphite_contacts_1}
        \end{center}
        {\footnotesize Park, Lieber, \textit{et. al.}, Nature Mat. 11, 120 (2012)}
    \end{onlyenv}
    \begin{onlyenv}<2>
        Experimental results show a \textbf{much larger} transfer length.
        \begin{center}
            \includegraphics[width=0.5\linewidth]{GonMe_exp_contacts_2}
        \end{center}
        \begin{center}
            \includegraphics[width=0.66\linewidth]{GonMe_exp_contacts_1}
        \end{center}
        {\footnotesize Xia, Avouris \textit{et. al.}, Nature Nanotechnology 6, 179 (2011)}
    \end{onlyenv}
    \begin{onlyenv}<3>
        \textbf{Hotspots} may be limiting current flow.
        \begin{center}
            \includegraphics[width=0.66\linewidth]{metal_graphene_hotspots}
        \end{center}
    \end{onlyenv}
\end{frame}

%------------------------------------------------

\section{Metal-Carbon parametrisation}
\subsection{Introduction}
\begin{frame}
    \frametitle{Goals}
    \begin{itemize}
        \item<1-> Simulate CVD-like \textbf{processes} of metal on top of graphene.
        \item<2-> Generate \textbf{realistic} structures to calculate R$_c$.
        \item<3-> \textbf{Parametrize} the interaction between Pd-C and Ni-C.
        \item<4-> Find a good forcefield that correctly describes the bond characteristics of metal-carbon pair.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{The Bond Order Potential}
    \begin{onlyenv}<1-2>
        \begin{block}<1-2>{Total Energy}
            \begin{align}
                E = \sum_{i>j} f_{ij}(r_{ij}) \left[ V^{ij}_R(r_{ij}) - \frac{b_{ij}+b_{ji}}{2} V^{ij}_A(r_{ij})\right]
            \end{align}
        \end{block}
        \begin{block}<2-2>{Cutoff Function}
            \begin{align}
                f(r) =
                \begin{cases}
                    1, &r \leq R-D, \\
                    \frac{1}{2}-\frac{1}{2} \sin\{ \pi (r-R)/2D \},  &\vert R-r \vert \leq D, \\
                    0, &r \geq R+D \\
                \end{cases}
            \end{align}
        \end{block}
    \end{onlyenv}
    \begin{onlyenv}<3->
        \begin{block}<3->{Angular dependency}
            \begin{align}
                b_{ij} &= (1+ \chi_{ij})^{-1/2}\\
                \chi_{ij} &= \sum_{k(\neq i,j)}\,f_{ik}(r_{ik})\,g_{ik}(\theta_{ijk})\,\exp\left[{2\mu_{ik}(r_{ij}-r_{ik})}\right] \\
                g(\theta) &= \gamma \left( 1+ \frac{c^2}{d^2} - \frac{c^2}{[d^2+(1+\cos{\theta})^2]}\right)
            \end{align}
        \end{block}
        \begin{block}<4->{Attractive and Repulsive potentials}
            \begin{align}
                V_R(r) &= \frac{D_0}{S-1}\exp\left[ -\beta \sqrt{2 S}\,\,(r-r_0)\right] \\
                V_A(r) &=  \frac{S\,D_0}{S-1}\exp\left[ -\beta \sqrt{2/S}\,\,(r-r_0)\right]
            \end{align}
        \end{block}
    \end{onlyenv}
    {\footnotesize Albe \textit{et. al.}, Phys. Rev. B 65, 195124}
\end{frame}

\subsection{Parametrization procedure}
\begin{frame}
    \frametitle{Computational Details}
    \begin{onlyenv}<1-2>
        \begin{block}<1->{Parameter set}
            \begin{align}
                \nonumber
                \textbf{X} = \left\{S, \beta, D_0, r_0, R, D, \gamma, c, d, \mu \right\}
            \end{align}
        \end{block}
        \begin{block}<2>{Target function}
            The \textbf{error function} for the potential is defined as
            \begin{align}
                \chi^2 (\textbf{X}) = \sum_i \sum_\nu \vert \partial V(\textbf{R}_i) / \partial q_\nu \vert^2
                + \sum_i \rho_i \left[ \Gamma_i - \Gamma_i^{(ref)}\right]^2
            \end{align}
            Optimizing this function will find a measure of the parameter set by evaluating how close the current
            configuration is to a reference geometry ($\textbf{R}_i$) and an adsoprtion energy reference ($\Gamma_i^{(ref)}$).
        \end{block}
        {\footnotesize F\"orster \textit{et. al.}, Phys. Rev. B 92, 165425}
    \end{onlyenv}
    \begin{onlyenv}<3>
        \begin{block}{Parallel Tempering Monte Carlo}
            Run several simulations at different temperatures using the Metropolis-Hastings algorithm to \textbf{explore}
            the parameter space.
            \begin{center}
                \includegraphics[width=0.7\linewidth]{PTMC_diagram_forster_thesis}
            \end{center}
        \end{block}
        {\footnotesize F\"orster \textit{et. al.}, Phys. Rev. B 92, 165425}
    \end{onlyenv}

    \begin{onlyenv}<4-8>
        The PTMC algorithm needs to be fed with target geometries and energies.
        \begin{block}{First principle calculations details}
            \begin{itemize}
                \item<4-> A set of \textbf{geometries} was used to obtain the parameters via the PTMC algorithm.
                \item<5-> Using First Principles calculations these geometries were relaxed until forces were lower than 0.01
                \AA/eV.
                \item<6-> Generalized Gradient Approximation functional in the parametrization of Perdew-Burke-Ernzerhof for Nickel.
                \item<7-> Local Density Approximation for Palladium.
                \item<8-> A double-$\zeta$ plus polarization basis taking into account spin polarization for Nickel.
            \end{itemize}
        \end{block}
    \end{onlyenv}
    \begin{onlyenv}<9->
        Training geometries
        \begin{center}
            \includegraphics<9>[width=0.49\linewidth]{ptmc_geometries/Ni4_lateral.jpg}
            \includegraphics<9>[width=0.49\linewidth]{ptmc_geometries/Ni4_top.jpg}
            \includegraphics<10>[width=0.49\linewidth]{ptmc_geometries/Ni13b_lateral.jpg}
            \includegraphics<10>[width=0.49\linewidth]{ptmc_geometries/Ni13b_top.jpg}
            \includegraphics<11>[width=0.49\linewidth]{ptmc_geometries/Ni38b_lateral.jpg}
            \includegraphics<11>[width=0.49\linewidth]{ptmc_geometries/Ni38b_top.jpg}
        \end{center}
        \begin{overprint}
            \onslide<9> Ni 4 (blue) on a graphene sheet (green)
            \onslide<10> Ni 13 (blue) on a graphene sheet (green)
            \onslide<11> Ni 55 (blue) on a graphene sheet (green)
        \end{overprint}
    \end{onlyenv}
\end{frame}

\subsection{Results and Conclusions}
\begin{frame}
    \frametitle{Results}
    \begin{onlyenv}<1-2>
        The BOP parameters for the Pt-C and Ni-C can be summarized in the table below
        \pause
        \begin{table}
            \centering
            \begin{tabular}{ |c|c|c| }
                \hline
                & Pd-C                 & Ni-C                 \\ \hline
                $S$      & 1.433$\times10^{2}$ & 1.432$\times10^2$ \\
                $\beta$  & 4.300 & 4.296 \\
                $D_0$    & 1.758 & 1.758 \\
                $r_0$    & 6.802$\times10^{-2}$ & 6.807$\times10^{-2}$ \\
                $R$      & 5.141 & 5.150 \\
                $D$      & 9.89$\times10^{-3}$ & 5.0$\times10^{-2}$ \\
                $\gamma$ & 1.806$\times10^{-2}$ & 1.800$\times10^{-2}$ \\
                $c$      & 6.903$\times10^2$ & 6.888$\times10^2$ \\
                $d$      & 4.755 & 4.753 \\
                $\mu$    & 2.803 & 2.807 \\
                \hline
            \end{tabular}
        \end{table}
    \end{onlyenv}
    \begin{onlyenv}<3>
        \begin{table}[h!]
            \centering
            \begin{tabular}{ |c|cc|cc| } \hline
                N$_a$  &   $E_\text{target}$(Pd) & $E_\text{reproduced}$(Pd) & $E_\text{target}$(Ni) & $E_\text{reproduced}$(Ni) \\ \hline
                1      &  -4.39458 & \textcolor{green}{ -4.02778} & -3.88876  & \textcolor{green}{-3.93083} \\ \hline
                2      &  -5.40951 & \textcolor{green}{ -5.29990} & -4.25306  & \textcolor{red}{-5.50767} \\ \hline
                3      &  -6.47832 & \textcolor{green}{ -6.14807} & -5.20541  & \textcolor{red}{-6.64512} \\ \hline
                4      &  -6.03174 & \textcolor{green}{ -6.36230} & -5.68362  & \textcolor{red}{-6.94597} \\ \hline
                7      & -10.52663 & \textcolor{red}{ -9.34759} & -7.44564 & \textcolor{red}{-9.06748} \\ \hline
                10     &  -9.44840 & \textcolor{green}{ -9.75142} & -7.95338  & \textcolor{red}{-9.00257} \\ \hline
                13a    &  -6.71057 & \textcolor{red}{ -9.31021} & -5.45038    & \textcolor{red}{-7.10955} \\ \hline
                13b    &  -6.52372 & \textcolor{green}{ -5.86262} & -7.25059  & \textcolor{red}{-8.69627} \\ \hline
                13c    &  -8.73059 & \textcolor{green}{ -9.15725} & -7.99246  & \textcolor{red}{-9.33724} \\ \hline
                38a    &  -9.14434 & \textcolor{green}{ -9.24890} & -7.44934  & \textcolor{green}{-7.39335} \\ \hline
                38b    &  -6.49479 & \textcolor{green}{ -6.99212} & -5.14229  & \textcolor{red}{-6.47318} \\ \hline
                55     &  -9.56981 & \textcolor{red}{-10.86422} & -7.20522    & \textcolor{red}{-8.06162} \\ \hline
            \end{tabular}
        \end{table}
    \end{onlyenv}
\end{frame}

\begin{frame}
    \frametitle{Conclusions}
    \begin{itemize}
        \item<1-> The Bond Order Potential is a potential that is able to orrectly describe Metal-Carbon interactions in
        classical simulations.
        \item<2-> Palladium and Nickel were not yet parametrized and are interesting due to their use with graphene.
        \item<3-> A set of parameters was found for both metals using the PTMC algorithm.
        \item<4-> Parameters seem to work fine with Pd; more improvement needed for Ni.
    \end{itemize}
    % Of course, this parameter set needs more testing before it can be released and needs to be validated agains
    % experimental data and theoretical calulation from first principles. This said, a realistic structure of metal
    % deposited on a graphene sheet can be carried out and its results studied. This simulations can be then used
    % in combination with tight-binding codes to calculate ballistic transport properties in the context of high
    % frequency applications.
    % The bond order potential in the interpretation of Albe \textit{et. al.} is the most suitable
    % force field to describe the Metal-Carbon interaction and a set of parameters was found for the interactions
    % Pd-C and Ni-C. The parametrisation of Pd-C and Ni-C interactions were studied for the suitability of metallic
    % contacts with graphene using the PTMC algorithm for parameter optimisation. This algorithm is a very efficient
    % tool for optimisation tasks that allows for a thorough search of the configuration space while able to find
    % optimal values on a certain region of this space. With this parameter set, it will be possible to simulate CVD-like
    % deposition of a metal on graphene, or use molecular dynamics for a first relaxation of large systems which would
    % not be suitable using first principles techniques.
\end{frame}

%------------------------------------------------

\section{Final remarks}
\begin{frame}
    \frametitle{Overall Conclusions}
    \begin{itemize}
        \item<1-> We used computational methods to gain a deeper understanding and build new physics knowledge.
        \item<2-> Using the insight offered by the drift-diffusion model it was possible to calculate depletion
        widths of a single layer MoS$_2$ $p-n$ junction. These results suggest that an analytic expression can be
        found to calculate the depletion width with closed expressions.
        \item<3-> The contact resistance between an electrode and the channel is crucial for determining the performance
        of HF devices. Using NEGF it was possible to create an atomistic structure of a graphite-graphene contact and
        calculate the contact resistance therein.
        \item<4-> Deeper understanding on electronic transfer from a metal to graphene is crucial towards process
        improvement on metal deposition on graphene. With the BOP parametrisation of the Pd-C and Ni-C pairs, it
        will be possible to simulate these kinds of depositions and further study the hotspot theory.
        \item<5-> Graphene is an outstanding material with interesting properties. Throughout this thesis it was possible
        to research with it and find uses on its applications in the HF field.
    \end{itemize}
\end{frame}

%------------------------------------------------

\begin{frame}
    \Huge{\centerline{Thank you for your attention.}}
\end{frame}

\end{document}
