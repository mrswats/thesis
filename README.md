Contact resistance and electrostatics of 2DFETs
===============================================

This is the repository for my PhD thesis.

Title: _Contact resistance and electrostatics of 2DFETs_

Author: Ferran Jovell Megias

Supervisor: Xavier Cartoix\`a Soler

Program: PhD in Electronic and Telecommunication engineering

Abstract: You can read it [here](https://gitlab.com/mrswats/thesis/-/jobs/artifacts/master/file/abstract_eng.pdf?job=pdf) in english or [here](https://gitlab.com/mrswats/thesis/-/jobs/artifacts/master/file/abstract_cat.pdf?job=pdf) in catalan.

Defense slides: You can find them [here](https://gitlab.com/mrswats/thesis/-/jobs/artifacts/master/file/defense.pdf?job=pdf)

Thesis: The full pdf of the thesis can be found [here](https://gitlab.com/mrswats/thesis/-/jobs/artifacts/master/file/master.pdf?job=pdf)


**ALL RIGHTS RESERVED**.
